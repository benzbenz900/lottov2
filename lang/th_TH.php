<?php
if (!defined("isdoc")){header('HTTP/1.1 404 Not Found');echo "Not Link Direct File"; exit;}

define('_home_', 'หน้าแรก');
define('_manager_', 'จัดการบัญชี');
define('_chang_pass_', 'เปลี่ยนรหัสผ่าน');
define('_top_up_', 'เติมเงินด้วยบัตรทรู');
define('_top_up_bank_', 'เติมเงินด้วยการโอน');
define('_logout_', 'ออกจากระบบ');
define('_regiter_', 'สมัครสมาชิก');
define('_login_', 'เข้าระบบ');
define('_admin_manager_', 'จัดการระบบ');
define('_oho_', 'โอ๊ะโอ่!');
define('_success_', 'สำเร็จ');
define('_error1_', 'ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง');
define('_error2_', 'อีเมล์นี้มีอยู่แล้ว');
define('_error3_', 'ชื่อผู้ใช้นี้มีอยู่แล้ว');
define('_error4_', 'อีเมล์หรือรหัสผ่านไม่ถูกต้อง หรืออาจมีผู้ใช้งาน ชื่อนี้อยู่แล้ว');
define('_error5_', 'รหัสผ่านไม่ตรงกัน กรุณาลองใหม่อีกครั้ง');
define('_error6_', 'ผู้ใช่ถูกบล็อกการเข้าใช่งาน กรุณาติดต่อ Admin ของเว็บเพื่อแก้ไข');
define('_succ1_', 'การสมัครของคุณเสร็จสมบูรณ์');
define('_sendmailpass_', 'ระบบได้ส่งรหัสผ่านใหม่ไปให้ทางอีกเมลเรียบร้อยแล้ว');
define('_buypage_', 'ซื้อเลขเด็ด');
define('_acc_name_', 'ชื่อบัญชี: ');
define('_acc_numb_', 'เลขบัญชี: ');
define('_acc_type_', 'ประเภทบัญชี: ');
define('_acc_bank_', 'ธนาคาร: ');
define('_widaw_', 'แจ้งถอนเงิน');
define('_my_bank_', 'บัญชีการเงินของฉัน');
define('_event_news_', 'ข่าวสาร');
define('_buynumber_', 'เลขที่ชื้อไว้ทั้งหมดใน 30 วัน');
define('_addcategory_text', 'เพิ่มหมวดหมู่เล่นเลข');
define('_addcategory_buttom', 'เพิ่มหมวดหมู่');
define('_editcategory_text', 'แก้ไขหมวดหมู่เล่นเลข');
define('_editcategory_buttom', 'แก้ไขหมวดหมู่');
define('_cons', 'ติดต่อเรา');
define('_rates', 'อัตตราจ่าย');
define('_resetpassword_', 'ลืมรหัสผ่าน');

?>