<?php
ob_start();
session_start();
function csrf_startup() {
    csrf_conf('rewrite-js', 'csrf-magic.js');
}
include 'csrf-magic.php';
/**
 * @package lnwPHP-application
*/

	/*
	PRODUCED BY:lnwPHP Thailand (lnwPHP Admin Manager)
	AUTHOR:Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	COPYRIGHT 2014 ALL RIGHTS RESERVED

	You must have purchased a valid license from lnwPHP.in.th in order to have
	access this file.

	You may only use this file according to the respective licensing terms
	you agreed to when purchasing this item.
	*/

	/** lnwPHP Application **/
define('isdoc',1);

/**
* @package load Config File
**/
include 'config.inc.php';

/**
* @package AutoLoad Setting Files
**/
include _setting_.'/setting.web.php';

/** lnwPHP Application **/

/**
* @package Run Application Class
**/
new lnw_run;

/**
 * @package lnwPHP-application
*/
ob_end_flush();
?>