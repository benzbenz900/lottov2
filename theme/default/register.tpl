<div class="col-md-8">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6"><h2>Register</h2></div>
        <div class="col-md-6"><h3 class="pull-right">{refshow}</h3></div>
      </div>
      <hr>
      {show_alert}
      <form class="form-horizontal" action="./register.html" method="post" role="form">
        <div class="form-group {emailerror}">
          <label class="col-sm-2 control-label">Email</label>
          <div class="col-sm-6">
          <input type="email" name="_email_lnwphp_in_th" class="form-control" placeholder="Email" value="{email}" required>
          </div>
        </div>
        <div class="form-group {usererror}">
          <label class="col-sm-2 control-label">Login Name</label>
          <div class="col-sm-6">
          <input type="text" name="_username_lnwphp_in_th" class="form-control" placeholder="Login Name" value="{username}" required>
          </div>
        </div>
        <div class="form-group {usererror}">
          <label class="col-sm-2 control-label">เบอร์มือถือ</label>
          <div class="col-sm-6">
          <input type="text" name="_phone_lnwphp_in_th" class="form-control" placeholder="เบอร์มือถือ" value="{phone_mobile}" required>
          </div>
        </div>
        <div class="form-group {passerror}">
          <label class="col-sm-2 control-label">รหัสผ่าน</label>
          <div class="col-sm-6">
            <input type="password" name="_password1_lnwphp_in_th" class="form-control" placeholder="รหัสผ่าน" required>
          </div>
        </div>
        <div class="form-group {passerror}">
          <label class="col-sm-2 control-label">ยืนยันรหัส</label>
          <div class="col-sm-6">
            <input type="password" name="_password2_lnwphp_in_th" class="form-control" placeholder="ยืนยันรหัส" required>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-6">
            <button type="submit" name="submit" class="btn btn-success">สมัคร</button> <button type="reset" class="btn btn-danger">ล้างค่า</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>