<div class="col-md-8">
	<div class="panel panel-default">
		<div class="panel-body">
			<h2>Check Number</h2>
			<hr>
			<form method="post">
			<div class="form-inline">
				<div class="form-group">
					<select onchange="getdaybuy();" name="optiontype" id="optiontype" class="form-control">
						<option>-- หมวดเลข --</option>
						{optiontype}
					</select>
				</div>
				<span id="optionku"></span>
				<div class="form-group">
					<input name="number" type="text" class="form-control" placeholder="เช่น 654" required >
				</div>
				<hr>
				<button type="submit" name="submit" class="btn btn-success btn-sm">ค้นหา</button>
				<button type="submit" name="submitall" class="btn btn-info btn-sm">ตรวจทั้งหมด</button>
			</div>
			</form>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>รายการ</th>
						<th>ผู้ชื้อ</th>
						<th>เลขที่เล่น</th>
						<th>อัตตราเล่น</th>
						<th>ประจำวันที่</th>
						<th>วันที่เวลาเล่น</th>
						<th>ตรวจรายการเอง</th>
					</tr>
				</thead>
				<tbody>
					{allnumber}
				</tbody>
			</table>
		</div>
	</div>

</div>

<script type="text/javascript">
	function success (uid, sid, nid) {
		swal({
			title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการปรับเป็นถูกรางวัล",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิก", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่",   closeOnConfirm: true }, function(){

				var xmlhttp;
				if (window.XMLHttpRequest){
					xmlhttp=new XMLHttpRequest();
				}else{
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function(){
					if (xmlhttp.readyState==4 && xmlhttp.status==200){
						if (xmlhttp.responseText == 1) {
							if (sid == 1) {
								xid = '<span class="label label-success">ถูกรางวัล</span>';
							}else if (sid == 2){
								xid = '<span class="label label-warning">ไม่ถูกรางวัล</span>';
							}else{
								xid = '<span class="label label-default">ไม่ทราบผล</span>';
							};
							document.getElementById('post_'+nid).innerHTML = xid;
						};
					}else{
						document.getElementById('post_'+nid).innerHTML = xmlhttp.responseText;
					}
				}
				xmlhttp.open("GET","./index.html?action=save_check&uid="+uid+"&sid="+sid+"&nid="+nid,true);
				xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
				xmlhttp.send();
			});
}

function getdaybuy () {
	id = document.getElementById('optiontype').value;
	var xmlhttp;
	if (window.XMLHttpRequest){
		xmlhttp=new XMLHttpRequest();
	}else{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('optionku').innerHTML = xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","./index.html?action=gethttp&id="+id,true);
	xmlhttp.send();
}
</script>