<div class="col-md-8">
  <div class="panel panel-default">
    <div class="panel-body">
    <h2>Reset Password</h2>
    <hr>
    {show_alert}
      <form class="form-horizontal" action="./resetpassword.html" method="post">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
          <div class="col-sm-10">
            <input type="text" name="email" class="form-control" id="inputEmail3" placeholder="Enter Email">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Reset Password</button> <a role="button" class="btn btn-success" href="./login.html">Login</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>