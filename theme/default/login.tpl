<div class="col-md-8">
  <div class="panel panel-default">
    <div class="panel-body">
    <h2>Login</h2>
    <hr>
    {show_alert}
      <form class="form-horizontal" action="./login.html" method="post">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Login Name</label>
          <div class="col-sm-10">
            <input type="text" name="login_name" class="form-control" id="inputEmail3" placeholder="Login Name">
          </div>
        </div>
        <div class="form-group">
          <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
          <div class="col-sm-10">
            <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
              <label>
                <input name="checkbox" type="checkbox" value="1"> Remember me
              </label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Sign in</button> <a role="button" class="btn btn-default" href="./resetpassword.html">Reset Password</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>