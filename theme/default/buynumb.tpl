<div class="col-md-8">
  <div class="panel panel-default">
    <div class="panel-body">
      <h2>{buy_number} {mfid} <u>จ่าย 1 บาท = {pay} บาท</u></h2>
      <hr>
      {show_alert}
      <center>
      <h3 class="text-success">งวดประจำวันที่ {daybuy}</h3>
      <h2 class="text-danger" id="showRemain"></h2>
      </center>
      <div class="row">
        <div class="col-md-9">
          <h4><code>ชื้อได้ไม่เกิน {aut} ต่อเลข</code></h4>
          <h4><code>{detail}</code></h4>
        </div>
        <div class="col-md-3">
          <div class="alert alert-success" role="alert">เงินคงเหลือ:<span id="money">{point_}</span></div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-9">
          <form class="form-horizontal" method="post" role="form">
            <input type="hidden" name="type" value="{type}">
            <div class="form-group">
              <label class="col-sm-2 control-label">เลขที่จะชื้อ</label>
              <div class="col-sm-10">
                <input type="text" name="number" class="form-control" placeholder="ใส่เลขที่จะชื้อ ({nub} ตัว)" maxlength="{nub}" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">อัตราเล่น</label>
              <div class="col-sm-10">
                <input onblur="ckifnumber();" id="salary" type="text" name="salary" class="form-control" placeholder="อัตราเล่น" required>
                จ่าย 1 บาท = {pay} บาท <code>เช่น 20 หรือ 3000 เป็นต้น หน่วยเป็น บาท</code>
              </div>
            </div>
            <script type="text/javascript">
              function ckifnumber () {
                sa = document.getElementById('salary').value;
                sa = sa*{pay};
                document.getElementById('salary_show').innerHTML = 'ถูกรางวัลรับ '+sa+' บาท';
              }
            </script>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="save_numb" class="btn btn-success">เพิ่มเลขที่จะชื้อ</button>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-3">
                <h3><span class="label label-info" id="salary_show"></span></h3>
              </div>
            </div>
          </form>
        </div>
      </div>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>รายการ</th>
            <th>เลขที่เล่น</th>
            <th>อัตตราเล่น</th>
            <th>ประจำวันที่</th>
            <th>วันที่เวลาเล่น</th>
            <th>ลบรายการ</th>
          </tr>
        </thead>
        <tbody>
          {show_buynumb}
        </tbody>
      </table>

    </div>
  </div>
</div>

<script type="text/javascript">
function countDown(){
  var timeA = new Date();
  var timeB = new Date("{tend}");
  var timeDifference = timeB.getTime()-timeA.getTime();    
  if(timeDifference>=0){
    timeDifference=timeDifference/1000;
    timeDifference=Math.floor(timeDifference);
    var wan=Math.floor(timeDifference/86400);
    var l_wan=timeDifference%86400;
    var hour=Math.floor(l_wan/3600);
    var l_hour=l_wan%3600;
    var minute=Math.floor(l_hour/60);
    var second=l_hour%60;
    var showPart=document.getElementById('showRemain');
    showPart.innerHTML="เหลือเวลา "+wan+" วัน "+hour+" ชั่วโมง "
    +minute+" นาที "+second+" วินาที"; 
      if(wan==0 && hour==0 && minute==0 && second==0){
        clearInterval(iCountDown);
        showPart.innerHTML="หมดเวลาชื้อแล้ว";
      }
  }else{
    var showPart=document.getElementById('showRemain');
    showPart.innerHTML="หมดเวลาชื้อแล้ว";
  }
}
var iCountDown=setInterval("countDown()",1000); 
</script>

<script src="theme/default/component/js/transition.js"></script>
<script src="theme/default/component/js/tooltip.js"></script>
<script src="theme/default/component/js/popover.js"></script>

<script>
  $(function () {
    $('.js-popover').popover()
  })
</script>

<script type="text/javascript">
  function del_post(id){
    swal({
      title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิกการลบ", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่ลบได้เลย",   closeOnConfirm: true }, function(){

        var xmlhttp;
        if (window.XMLHttpRequest){
          xmlhttp=new XMLHttpRequest();
        }else{
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
          if (xmlhttp.readyState==4 && xmlhttp.status==200){
            ids = xmlhttp.responseText.split('|');
            if (ids[1] == '1') {
            money = document.getElementById('money').innerHTML;
            document.getElementById('money').innerHTML = ids[0];
            document.getElementById("post_"+id).remove();
            }else if (ids[1] == '2') {
              swal({   title: "ไม่สามารถยกเลิกได้!",   text: "ขออภัย ไม่สามารถยกเลิกได้ เนื่องจากเกินเวลาที่กำหนด "+ids[2]+" นาที ไปแล้ว ไม่สามารถยกเลิกได้",   type: "error",confirmButtonText: "OK" });
            }else{
               swal({   title: "ขออภัย!",   text: "เกินขอผิดพลาดที่ไม่ทราบสาเหตุ ("+xmlhttp.responseText+")",   type: "error",confirmButtonText: "OK" });
            }
          }
        }
        xmlhttp.open("GET","./index.html?action=del&d=_lnwphp_save_numb_&t=id&i=" + id,true);
        xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xmlhttp.send();
      });
  }
  function success (uid, sid, nid) {
    swal({
      title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการปรับเป็นถูกรางวัล",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิก", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่",   closeOnConfirm: true }, function(){

        var xmlhttp;
        if (window.XMLHttpRequest){
          xmlhttp=new XMLHttpRequest();
        }else{
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
          if (xmlhttp.readyState==4 && xmlhttp.status==200){
            if (xmlhttp.responseText == 1) {
              if (sid == 3) {
                xid = '<span class="label label-default">รับเงินแล้ว</span>';
              }else{
                xid = '<span class="label label-info">ไม่ทราบผล</span>';
              };
              document.getElementById('post_'+nid).innerHTML = xid;
            };
          }else{
            document.getElementById('post_'+nid).innerHTML = xmlhttp.responseText;
          }
        }
        xmlhttp.open("GET","./index.html?action=save_check&uid="+uid+"&sid="+sid+"&nid="+nid,true);
        xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xmlhttp.send();
      });
}
</script>