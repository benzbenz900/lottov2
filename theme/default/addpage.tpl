<div class="col-md-8">
	
	<div class="panel panel-default">
		<div class="panel-body">
			<h2>Admin Panel Add Page</h2>
			<hr>
			{admin_alert}
			<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
			<form method="post">
				<div class="form-group">
					<label>หัวเรื่อง</label>
					<input name="name" type="text" class="form-control" placeholder="หัวเรื่อง">
				</div>
				<div class="form-group">
					<label>ประเภท</label>
					<select name="type" class="form-control">
						<option value="-1">--เลือก--</option>
						<option {selected0} value="0">หน้าเว็บ</option>
						<option {selected1} value="1">ข่าวสาร</option>
						<option {selected2} value="2">โปรโมชั่น</option>
						<option {selected3} value="3">กิจกรรม</option>
						<option {selected4} value="4">บทความหน้าแรก</option>
					</select>
				</div>
				<div class="form-group">
					<label>รายละเอียด</label>
					<textarea name="detail" class="form-control" placeholder="รายละเอียด"></textarea>
				</div>
				<button name="save_addpage" type="submit" class="btn btn-success">เพิ่มหน้านี้ลงในเว็บ</button>
			</form>
			<script>
				CKEDITOR.replace('detail');
			</script>
		</div>
	</div>

</div>