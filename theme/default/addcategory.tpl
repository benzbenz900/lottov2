<div class="col-md-8">
	<div class="panel panel-default">
		<div class="panel-body">
			<h2>{addcategory_text} <a class="btn btn-large btn-success" href="cmd@addcategory.admin">เพิ่ม</a></h2>
			<hr>
			<div class="row" {sty}>
				<div class="col-md-7">
					<h3>หมวดหมู่เลขเล่น</h3>
					<div class="form-group">
						<label>ชื่อหมวดหมู่</label>
						<input id="name" class="form-control" name="name" type="text" placeholder="เช่น 3 ตัวบน" value="{name}" required>
					</div>
					<div class="form-group">
						<label>จำนวนกรอกเลข</label>
						<input id="numbernub" class="form-control" name="numbernub" type="number" placeholder="เช่น กรอกเลขได้ 3 ก็ใส่ 3" value="{numbernub}" required>
					</div>
					<div class="form-group">
						<label>รายละเอียด</label>
						<textarea id="detail" rows="3" name="detail" class="form-control" placeholder="เช่น ขอจำกัด หรือวิธีชื้อ">{detail}</textarea>
					</div>
					<div class="form-group">
						<label>วันที่เริ่มชื้อได้</label>
						<input id="time_limit" class="form-control" name="time_limit" type="text" placeholder="ใส่วันที่และเวลา" value="{time_limit}" required>
					</div>
					<div class="form-group">
						<label>วันที่หมดเวลาชื้อ</label>
						<input id="time_limitd" class="form-control" name="time_limitd" type="text" placeholder="ใส่วันที่และเวลา" value="{time_limitd}" required>
					</div>
					<div class="form-group">
						<label>งวดวันที่ เช่น 2015-09-16</label>
						<input id="daybuy" class="form-control" name="daybuy" type="text" placeholder="เช่น 2015-09-16" value="{daybuy}" required>
					</div>
					<div class="form-group">
						<label>จำกัดการชื้อต่อ 1 เลข</label>
						<input id="numberaut" class="form-control" name="numberaut" type="text" placeholder="เช่น 4000" value="{numberaut}" required>
					</div>
					<div class="form-group">
						<label>อัตตราจ่าย 1 บาท เท่ากับ = ?</label>
						<input value="{m_success}" id="m_success" class="form-control" name="m_success" type="text" placeholder="เช่น 70">
					</div>
					<button onclick="se_null();" type="button" class="btn btn-large btn-success">{addcategory_buttom}</button>
				</div>
			</div>
			<h3>รายการหมวดหมู่หวย</h3>
			<span id="showjs"></span>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ชื่อหมวดหมู่</th>
						<th>จำกัดเวลาชื้อ</th>
						<th>ชื้อไม่เกิน</th>
						<th>อัตตราจ่าย</th>
						<th>#</th>
					</tr>
				</thead>
				<tbody id="mybank">
					{mycategory}
				</tbody>
			</table>

		</div>

	</div>
</div>

</div>

<script type="text/javascript">
	function se_null () {
		name = document.getElementById('name').value;
		detail = document.getElementById('detail').value;
		time_limit = document.getElementById('time_limit').value;
		time_limitd = document.getElementById('time_limitd').value;
		daybuy = document.getElementById('daybuy').value;
		m_success = document.getElementById('m_success').value;
		numbernub = document.getElementById('numbernub').value;
		numberaut = document.getElementById('numberaut').value;
		var xmlhttp;
		if (window.XMLHttpRequest){
			xmlhttp=new XMLHttpRequest();
		}else{
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function(){
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				if (xmlhttp.responseText == "1") {
					swal("สำเร็จ!", "บันทึกข้อมูลบัญชีเรียบร้อยแล้ว", "success");
					document.getElementById('name').value = '';
					document.getElementById('detail').value = '';
					document.getElementById('time_limit').value = '';
					document.getElementById('time_limitd').value = '';
					document.getElementById('m_success').value = '';
					document.getElementById('daybuy').value = '';
					document.getElementById('edit').remove();
					document.getElementById('numbernub').value = '';
					document.getElementById('numberaut').value = '';
					window.load_table();
				}else{
					swal({   title: "ขออภัย!",   text: "ขออภัย การบันทึกไม่สามารถทำได้ กรุณาลองอีกครั้ง"+xmlhttp.responseText,   type: "error",confirmButtonText: "OK" });
				};
			}
		}
		xmlhttp.open("GET","./index.html?action={add_edit}_category&name="+encodeURI(name)+"&detail="+encodeURI(detail)+"&time="+encodeURI(time_limit)+"&day="+encodeURI(time_limitd)+"&mmm="+encodeURI(m_success){set_id}+"&nub="+encodeURI(numbernub)+"&aut="+encodeURI(numberaut)+"&daybuy="+encodeURI(daybuy),true);
		xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xmlhttp.send();
	}

	function del_post(id){
		swal({
			title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิกการลบ", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่ลบได้เลย",   closeOnConfirm: true }, function(){

				var xmlhttp;
				if (window.XMLHttpRequest){
					xmlhttp=new XMLHttpRequest();
				}else{
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function(){
					if (xmlhttp.readyState==4 && xmlhttp.status==200){
						document.getElementById('showjs').innerHTML = xmlhttp.responseText;
					}
				}
				xmlhttp.open("GET","./index.html?action=del&d=_lnwphp_number_type_&t=id&i=" + id,true);
				xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
				xmlhttp.send();
				document.getElementById("post_"+id).remove();
			});
	}

	function load_table () {
		var xmlhttp;
		if (window.XMLHttpRequest){
			xmlhttp=new XMLHttpRequest();
		}else{
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function(){
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById('mybank').innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","./index.html?action=load_category",true);
		xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xmlhttp.send();
	}
</script>