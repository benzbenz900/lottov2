<div class="col-md-8">
	<div class="panel panel-default">
		<div class="panel-body">
		<small><span class="label label-primary">{view} View</span> <span class="label label-info">Post Time {date}</span></small>
		<h2>{name}</h2>
		<hr>
		{detail}
		</div>
	</div>
</div>