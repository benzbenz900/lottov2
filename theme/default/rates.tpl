<div class="col-md-8">
  <div class="panel panel-default">
    <div class="panel-body">
      <h2>อัตตราการจ่าย</h2>
      อัตตราการจ่ายเงินต่อบาทล่ะตามรายละเอียดในตาราง
      <hr>
      <table class="table table-striped">
      <thead>
        <tr>
          <th>หมวดเลข</th>
          <th>บาทละ</th>
          <th>งวดวันที่</th>
          <th>หมดเวลาแทง</th>
        </tr>
      </thead>
       {showrates}
     </table>
   </div>
 </div>
</div>