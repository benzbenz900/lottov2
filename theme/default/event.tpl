<div class="col-md-8">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#all_news" data-toggle="tab">All News</a></li>
		<li><a href="#promotion" data-toggle="tab">Promotion</a></li>
		<li><a href="#events" data-toggle="tab">Events</a></li>
		<li><a href="#photo_events" data-toggle="tab">Photo Events</a></li>
		<li><a href="#screenshot" data-toggle="tab">Screenshot</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade in active" id="all_news">

			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							{news_show}
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="tab-pane fade" id="promotion">

			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							{promotiom_show}
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="tab-pane fade" id="events">

			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							{events_show}
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="tab-pane fade" id="photo_events">

			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							{events_photo_show}
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="tab-pane fade" id="screenshot">

			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							{screenshot_show}
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>