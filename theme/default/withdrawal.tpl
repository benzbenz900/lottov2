<div class="col-md-8">

  <div class="panel panel-default">
    <div class="panel-body">
      <h2>ถอนเงิน Withdrawal</h2>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-danger">
            <strong>คำเตือน</strong> กรุณาอ่าน <a href="#terms" data-toggle="tab"><b>ข้อตกลง</b></a>ในการถอนเงินจากเรา
          </div>
        </div>
      </div>
      <div class="row">
        <div class="tab-content">
          <div class="tab-pane fade in active" id="topup">
            <div class="col-md-12">
              <span id="showjs"></span>
            </div>
            <div class="col-md-7">
              <h3>แจ้งการถอนเงิน</h3>
              <div class="form-group">
                <label>บัญชีที่ให้โอนเข้า</label>
                <select id="se_l" class="form-control" name="acc_withdrawal">
                  <option value="">-- เลือก --</option>
                  {show_withdrawal_select}
                </select>
              </div>
              <div class="form-group">
                <label>จำนวนเงินที่โอน</label>
                <input id="amount" class="form-control" name="amount" type="text" placeholder="เช่น 1500.55" required>
              </div>
              <div class="form-group">
                <label>Login Name</label>
                <input id="user" class="form-control" name="login_name" type="text" placeholder="Login Name" value="{login_name}" required>
              </div>
              <div class="form-group">
                <label>Email</label>
                <input id="email" class="form-control" name="email" type="email" placeholder="Email" value="{email}" required>
              </div>
              <a class="btn btn-danger" href="#terms" data-toggle="tab">ข้อตกลง</a> <button onclick="se_null();" type="button" class="btn btn-large btn-success">แจ้งถอนเงิน</button>
            </div>
            <div class="col-md-5">
              <h3>ยอดเงินคงเหลือ <span class="label label-danger" id="money_point_my_you">{money_point_my_you}</span></h3>
            </div>
            <div class="col-md-12">
              <h3>รายการแจ้งถอนเงินของฉัน</h3>
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>วันที่</th>
                    <th>ธนาคาร</th>
                    <th>จำนวน</th>
                    <th>สถานะ</th>
                    <th>สถานะ</th>
                  </tr>
                </thead>
                <tbody id="list_out_money">
                  {list_out_money}
                </tbody>
              </table>

            </div>
          </div>
          <div class="tab-pane fade in" id="terms">
            <div class="col-md-12">
              {terms}
              <hr>
              <a class="btn btn-success" href="#topup" data-toggle="tab">ยอมับเงื่อนไข</a> <a class="btn btn-danger" href="./index.html">ไม่ยอมรับเงื่อนไข</a>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

</div>

<script type="text/javascript">
  function se_null () {
    x = document.getElementById('se_l').value;
    d = document.getElementById('amount').value;
    user = document.getElementById('user').value;
    email = document.getElementById('email').value;
    if (x == "") {
      swal({   title: "บัญชีที่โอนเข้า!",   text: "กรุณาเลือก ธนาคารที่ทำการโอนด้วย",   type: "error",   confirmButtonText: "OK" });
    }else if(d == ""){
      swal({   title: "จำนวนเงินที่โอน!",   text: "กรุณาใส่จำนวนเงิน ที่โอน ก่อน",   type: "error",   confirmButtonText: "OK" });
    }else{
      var xmlhttp;
      if (window.XMLHttpRequest){
        xmlhttp=new XMLHttpRequest();
      }else{
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState==4 && xmlhttp.status==200){
          sm = xmlhttp.responseText;
          sm = sm.split('|');
          if (sm[0] == "1") {
            swal("สำเร็จ!", "บันทึกข้อมูลการแจ้งถอนเงินเรียบร้อยแล้ว", "success");
            document.getElementById('money_point_my_you').innerHTML = sm[1];
            document.getElementById('se_l').value = "";
            document.getElementById('amount').value = "";
            window.load_table();
          }else if(sm[0] == "2"){
            swal({   title: "ขออภัย!",   text: "ขออภัย การบันทึกไม่สามารถทำได้ กรุณาลองอีกครั้ง",   type: "error",confirmButtonText: "OK" });
          }else if(sm[0] == "3"){
            swal({   title: "ขออภัย!",   text: "ขออภัย ยอดเงินในบัญชีของคุณน้อยกว่า "+d+" ไม่สามารถถอนเงินตามจำนวนนี้ได้",   type: "error",confirmButtonText: "OK" });
          }else if(sm[0] == "4"){
            swal({   title: "แจ้งได้ 1 ครั้ง!",   text: "ขออภัย แจ้งการโอนเงินได้ 1 ครั้งเท่านั้น กรุณารอจนกว่าจะได้รับการยืนยันยอดเงิน แล้วจะสามารถแจ่งการโอนได้อีกครั้ง",   type: "error",confirmButtonText: "OK" });
          };
        }
      }
      xmlhttp.open("GET","./index.html?action=save_out_money&uid={user_id}&u="+user+"&e="+email+"&p="+d+"&t={time_date_now}&b="+x+"&i={ip_user}",true);
      xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xmlhttp.send();
    };
  }

  function del_post(id){
    swal({
      title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิกการลบ", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่ลบได้เลย",   closeOnConfirm: true }, function(){

        var xmlhttp;
        if (window.XMLHttpRequest){
          xmlhttp=new XMLHttpRequest();
        }else{
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
          if (xmlhttp.readyState==4 && xmlhttp.status==200){
            sm = xmlhttp.responseText;
            sm = sm.split('|');
            document.getElementById('money_point_my_you').innerHTML = sm[0];
            document.getElementById('showjs').innerHTML = sm[1];
          }
        }
        xmlhttp.open("GET","./index.html?action=del&d=_lnwphp_save_withdrawal_&t=id&i=" + id,true);
        xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xmlhttp.send();
        document.getElementById("post_"+id).remove();
      });
}

function load_table () {
  var xmlhttp;
  if (window.XMLHttpRequest){
    xmlhttp=new XMLHttpRequest();
  }else{
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function(){
    if (xmlhttp.readyState==4 && xmlhttp.status==200){
      document.getElementById('list_out_money').innerHTML = xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","./index.html?action=load_table_widaw",true);
  xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xmlhttp.send();
}
</script>