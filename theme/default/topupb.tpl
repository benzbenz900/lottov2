<div class="col-md-8">
	
	<div class="panel panel-default">
		<div class="panel-body">
			<h2>TopUP Money Bank</h2>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-danger">
						<strong>คำเตือน</strong> กรุณาอ่าน <a href="#terms" data-toggle="tab"><b>ข้อตกลง</b></a>ในการเติมเงินของเรา
					</div>
				</div>
			</div>
			<div class="row">
				<div class="tab-content">
					<div class="tab-pane fade in active" id="topup">

						<div class="col-md-12">
							<div class="alert alert-success">
								<h3>รายละเอียดบัญชีธาคาร</h3>
								{show_bank_detail}
							</div>
						</div>

						<div class="col-md-7">
							<h3>แจ้งการโอนเงิน</h3>
							<div class="form-group">
								<label>บัญชีที่โอนเข้า</label>
								<select id="se_l" class="form-control" name="acc_bank">
									<option value="">-- เลือก --</option>
									{show_select}
								</select>
							</div>
							<div class="form-group">
								<label>วันที่และเวลาที่โอน</label>
								<input id="date_time" class="form-control" name="dt" type="text" placeholder="เช่น 30/12/58 14:30" value="{time_date_now}" required>
							</div>
							<div class="form-group">
								<label>จำนวนเงินที่โอน</label>
								<input id="amount" class="form-control" name="amount" type="text" placeholder="เช่น 1500.55" required>
							</div>
							<div class="form-group">
								<label>Login Name</label>
								<input id="user" class="form-control" name="login_name" type="text" placeholder="Login Name" value="{login_name}" required>
							</div>
							<div class="form-group">
								<label>Email</label>
								<input id="email" class="form-control" name="email" type="email" placeholder="Email" value="{email}" required>
							</div>
							<div class="form-group">
								<label>หมายเหตุอื่นๆ หากต้องการแนบหลักฐานกรุณาฝากรูปแล้วใส่ลิ้งรูปที่นี้</label>
								<input class="form-control" name="remark" type="text" value="-" placeholder="หมายเหตุอื่นๆ">
							</div>
							<a class="btn btn-danger" href="#terms" data-toggle="tab">ข้อตกลง</a> <button onclick="se_null();" type="button" class="btn btn-large btn-success">แจ้งเติมเงิน</button>
						</div>
					</div>
					<div class="tab-pane fade in" id="terms">
						<div class="col-md-12">
							{terms}
							<hr>
							<a class="btn btn-success" href="#topup" data-toggle="tab">ยอมับเงื่อนไข</a> <a class="btn btn-danger" href="./index.html">ไม่ยอมรับเงื่อนไข</a>
						</div>
					</div>
				</div>
			</div>
			<h3>รายการเติมเงินของฉัน</h3>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ผู้เติม</th>
						<th>บัตรราคา</th>
						<th>IP ผู้เติม</th>
						<th>รหัสของบัตร</th>
						<th>เติมเมื่อ</th>
						<th>เติมเติมจาก</th>
						<th>#</th>
					</tr>
				</thead>
				<tbody>
					{topuplist}
				</tbody>
			</table>

			<div class="row">
				<div class="col-md-12">
					<hr>
					<center>
						<div class="alert alert-danger">
							<strong>กรุณาอ่านเงื่อนไขต่างๆก่อนการเติมเงินทุกครั้ง</strong> หากเติมเงินเข้ามาแล้ว เราถือว่าท่านยอมรับเงื่อนไขทุกประการ
						</div>
					</center>
				</div>
			</div>
		</div>

	</div>
</div>

</div>

<script type="text/javascript">
	function se_null () {
		x = document.getElementById('se_l').value;
		d = document.getElementById('amount').value;
		date_time = document.getElementById('date_time').value;
		user = document.getElementById('user').value;
		email = document.getElementById('email').value;
		if (x == "") {
			swal({   title: "บัญชีที่โอนเข้า!",   text: "กรุณาเลือก ธนาคารที่ทำการโอนด้วย",   type: "error",   confirmButtonText: "OK" });
		}else if(d == ""){
			swal({   title: "จำนวนเงินที่โอน!",   text: "กรุณาใส่จำนวนเงิน ที่โอน ก่อน",   type: "error",   confirmButtonText: "OK" });
		}else{
			var xmlhttp;
			if (window.XMLHttpRequest){
				xmlhttp=new XMLHttpRequest();
			}else{
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function(){
				if (xmlhttp.readyState==4 && xmlhttp.status==200){
					if (xmlhttp.responseText == "1") {
						swal("สำเร็จ!", "บันทึกข้อมูลการแจ้งโอนเรียบร้อยแล้ว", "success");
						document.getElementById('se_l').value = "";
						document.getElementById('amount').value = "";
						document.getElementById('date_time').value = "";
					}else if (xmlhttp.responseText == "3") {
						swal({   title: "แจ้งได้ 1 ครั้ง!",   text: "ขออภัย แจ้งการโอนเงินได้ 1 ครั้งเท่านั้น กรุณารอจนกว่าจะได้รับการยืนยันยอดเงิน แล้วจะสามารถแจ่งการโอนได้อีกครั้ง",   type: "error",confirmButtonText: "OK" });
					}else{
						swal({   title: "ขออภัย!",   text: "ขออภัย การบันทึกไม่สามารถทำได้ กรุณาลองอีกครั้ง",   type: "error",confirmButtonText: "OK" });
					};
				}
			}
			xmlhttp.open("GET","./index.html?action=save_in_money&u="+user+"&e="+email+"&p="+d+"&t="+date_time+"&b="+x+"&i={ip_user}",true);
			xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
			xmlhttp.send();
		};
	}
	
	function del_post(id)
	{

		swal({
			title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิกการลบ", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่ลบได้เลย",   closeOnConfirm: true }, function(){

				var xmlhttp;
				if (window.XMLHttpRequest){
					xmlhttp=new XMLHttpRequest();
				}else{
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function(){
					if (xmlhttp.readyState==4 && xmlhttp.status==200){
						document.getElementById("showjs").innerHTML=xmlhttp.responseText;
					}
				}
				xmlhttp.open("GET","./index.html?action=del&d=_lnwphp_log_&t=id&i=" + id,true);
				xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
				xmlhttp.send();

				document.getElementById("post_"+id).remove();
			});
	}
</script>