<div class="col-md-8">
  <div class="panel panel-default">
    <div class="panel-body">
    <h2>รายการบัตรทรู</h2>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>ชื่อแสดง</th>
                <th>ราคาบัตร/บาท</th>
                <th>จำนวนเงินที่ได้ในเว็บ</th>
                <th>แก้ไข/ลบ</th>
              </tr>
            </thead>
            <tbody>
              {showuser}
            </tbody>
          </table>

    </div>
  </div>
</div>

<script type="text/javascript">
  function del_post(id){
    swal({
      title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิกการลบ", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่ลบได้เลย",   closeOnConfirm: true }, function(){

        var xmlhttp;
        if (window.XMLHttpRequest){
          xmlhttp=new XMLHttpRequest();
        }else{
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
          if (xmlhttp.readyState==4 && xmlhttp.status==200){
            document.getElementById('showjs').innerHTML = xmlhttp.responseText;
          }
        }
        xmlhttp.open("GET","./index.html?action=del&d=_lnwphp_point_&t=id&i=" + id,true);
        xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xmlhttp.send();
        document.getElementById("post_"+id).remove();
      });
  }
</script>