<div class="col-md-8">
	
	<div class="panel panel-default">
		<div class="panel-body">
			<h2>บัญชีธนาคารของฉัน</h2>
			<hr>
			<div class="row">
				<div class="col-md-7">
					<h3>บัญชีธนาคารของฉัน</h3>
					<div class="form-group">
						<label>ชื่อบัญชี</label>
						<input id="acc_name" class="form-control" name="acc_name" type="text" placeholder="เช่น นาย ธนชิต สิทธิกัน" value="" required>
					</div>
					<div class="form-group">
						<label>ประเภทบัญชี</label>
						<input id="acc_type" class="form-control" name="acc_type" type="text" placeholder="เช่น ออมทรัพย์" value="" required>
					</div>
					<div class="form-group">
						<label>ธนาคาร</label>
						<input id="acc_bank" class="form-control" name="acc_bank" type="text" placeholder="เช่น กรุงไทย" required>
					</div>
					<div class="form-group">
						<label>เลขที่บัญชี</label>
						<input id="acc_numb" class="form-control" name="acc_numb" type="text" placeholder="เช่น 842-008-5987" required>
					</div>
					<button onclick="se_null();" type="button" class="btn btn-large btn-success">เพิ่มบัญชี</button>
				</div>
			</div>
			<h3>รายการบัญชีธนาคารของฉัน</h3>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ชื่อบัญชี</th>
						<th>ประเภทบัญชี</th>
						<th>ธนาคาร</th>
						<th>เลขที่บัญชี</th>
						<th>#</th>
					</tr>
				</thead>
				<tbody id="mybank">
					{mybanklist}
				</tbody>
			</table>

		</div>

	</div>
</div>

</div>

<script type="text/javascript">
	function se_null () {
		acc_name = document.getElementById('acc_name').value;
		acc_type = document.getElementById('acc_type').value;
		acc_bank = document.getElementById('acc_bank').value;
		acc_numb = document.getElementById('acc_numb').value;
		if (acc_name == "") {
			swal({   title: "ชื่อบัญชี!",   text: "กรุณาเลือก ใส่ชื่อบัญชีด้วย",   type: "error",   confirmButtonText: "OK" });
		}else if(acc_type == ""){
			swal({   title: "ประเภทบัญชี!",   text: "กรุณาใส่ประเภทบัญชีก่อน",   type: "error",   confirmButtonText: "OK" });
		}else if(acc_bank == ""){
			swal({   title: "ธนาคาร!",   text: "กรุณาใส่ธนาคารก่อน",   type: "error",   confirmButtonText: "OK" });
		}else if(acc_numb == ""){
			swal({   title: "เลขที่บัญชี!",   text: "กรุณาใส่เลขที่บัญชีก่อน",   type: "error",   confirmButtonText: "OK" });
		}else{
			var xmlhttp;
			if (window.XMLHttpRequest){
				xmlhttp=new XMLHttpRequest();
			}else{
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function(){
				if (xmlhttp.readyState==4 && xmlhttp.status==200){
					if (xmlhttp.responseText == "1") {
						swal("สำเร็จ!", "บันทึกข้อมูลบัญชีเรียบร้อยแล้ว", "success");
						document.getElementById('acc_name').value = '';
						document.getElementById('acc_type').value = '';
						document.getElementById('acc_bank').value = '';
						document.getElementById('acc_numb').value = '';
						window.load_table();
					}else{
						swal({   title: "ขออภัย!",   text: "ขออภัย การบันทึกไม่สามารถทำได้ กรุณาลองอีกครั้ง",   type: "error",confirmButtonText: "OK" });
					};
				}
			}
			xmlhttp.open("GET","./index.html?action=save_mybank&id_user={user_id}&acc_name="+encodeURI(acc_name)+"&acc_numb="+encodeURI(acc_numb)+"&acc_type="+encodeURI(acc_type)+"&acc_bank="+encodeURI(acc_bank)+"",true);
			xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
			xmlhttp.send();
		};
	}

	function del_post(id){
		swal({
			title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิกการลบ", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่ลบได้เลย",   closeOnConfirm: true }, function(){

				var xmlhttp;
				if (window.XMLHttpRequest){
					xmlhttp=new XMLHttpRequest();
				}else{
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function(){
					if (xmlhttp.readyState==4 && xmlhttp.status==200){
						document.getElementById('showjs').innerHTML = xmlhttp.responseText;
					}
				}
				xmlhttp.open("GET","./index.html?action=del&d=_lnwphp_user_bank_&t=id&i=" + id,true);
				xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
				xmlhttp.send();
				document.getElementById("post_"+id).remove();
			});
	}

	function load_table () {
		var xmlhttp;
		if (window.XMLHttpRequest){
			xmlhttp=new XMLHttpRequest();
		}else{
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function(){
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById('mybank').innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","./index.html?action=load_mybank",true);
		xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xmlhttp.send();
	}
</script>