<div class="col-md-8">

  <div class="panel panel-default">
    <div class="panel-body">

    <h2>Change Password</h2>
    <hr>
    {show_alert}
      <form class="form-horizontal" method="post">
        <div class="form-group {epo}">
          <label for="inputPassword3" class="col-sm-3 control-label">Old Password</label>
          <div class="col-sm-9">
            <input type="password" name="password_old" class="form-control" id="inputPassword3" placeholder="Old Password">
          </div>
        </div>
        <div class="form-group {ep1}">
          <label for="inputPassword3" class="col-sm-3 control-label">New Password</label>
          <div class="col-sm-9">
            <input type="password" name="password_new1" class="form-control" id="inputPassword3" placeholder="New Password">
          </div>
        </div>
        <div class="form-group {ep2}">
          <label for="inputPassword3" class="col-sm-3 control-label">New Password 2</label>
          <div class="col-sm-9">
            <input type="password" name="password_new2" class="form-control" id="inputPassword3" placeholder="New Password 2">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Change Password</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>