<div class="col-md-8">
	<div class="panel panel-default">
		<div class="panel-body">
			<h2>User Page</h2>
			<hr>
			<div class="col-md-offset-4">
				<strong>Login Name:</strong> {login_name}<br>
				<strong>Email:</strong> {email}<br>
				<strong>Password:</strong> **********<br>
				<strong>Registered Date:</strong> {date_register}<br>
				<strong>Money:</strong> <span class="label label-success">{user_point}</span><br>
				<strong>Phone:</strong> {phone}</span>
			</div>
			<center><a class="btn btn-sm btn-primary" href="./edituser.html"><span class="glyphicon glyphicon-user"></span> แก้ไขข้อมูลส่วนตัว</a></center>
			<div class="row">
				<div class="col-md-12">
				<center><h4>URL แนะนำสมาชิก รับค่าคอมมิชั่น <span class="label label-success">{percent}%</span></h4></center>
				<center><span class="text-danger">{useraf}</span></center><br>
				<center>
				<p>จำนวนค่าคอมที่จะได้รับหลังวันหวยออก <span class="label label-success">{commissions}</span> (อาจจะขึ้นลงตามจำนวนคนชื้อเลขและยกเลิกการชื้อ)</p>
				{show_getcom}
				</center>
				</div>
			</div>
			<br>
			<center>{show_menu}</center>
			<table class="table">
			<h2>ผู้ใช้ที่คุณแนะนำมา</h2>
				<thead>
					<tr>
						<th>Username</th>
						<th>Email</th>
						<th>วันที่เข้าร่วม</th>
					</tr>
				</thead>
				<tbody>
					{refshow}
				</tbody>
			</table>
			<table class="table">
			<h2>ค่าคอมจากการชื้อเลข {percent}%</h2>
				<thead>
					<tr>
						<th>From Username</th>
						<th>Buy Date Time</th>
						<th>Full Price</th>
						<th>Commission</th>
					</tr>
				</thead>
				<tbody>
					{commission_ref_show}
				</tbody>
			</table>
		</div>
	</div>
</div>