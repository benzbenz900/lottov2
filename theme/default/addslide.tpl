<div class="col-md-8">
	
	<div class="panel panel-default">
		<div class="panel-body">
			<h2>Admin Panel Add Slide</h2>
			<hr>
			<span id="showjs"></span>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>หัวเรื่อง</th>
						<th>URL</th>
						<th>ลบ</th>
					</tr>
				</thead>
				<tbody>
					{slide_show}
				</tbody>
			</table>
			
			{admin_alert}
			<form enctype="multipart/form-data" method="post" role="form"> 
				<div class="form-group"> 
					<label>ชื่อเรื่อง</label>
					<input type="text" name="name" class="form-control" placeholder="ชื่อเรื่อง">
				</div> 
				<div class="form-group"> 
					<label>รายละเอียด</label>
					<input type="text" name="detail" class="form-control" placeholder="รายละเอียด">
				</div> 
				<div class="form-group"> 
					<label>IMG</label>
					<input class="form-control" type="file" name="img">
				</div> 
				<div class="form-group"> 
					<label>ลิ้ง URL</label>
					<input type="text" name="url" class="form-control" placeholder="ลิ้ง URL">
				</div> 
				<div class="form-group"> 
					<label>แสดงผล</label>
					<select class="form-control" name="show"> 
						<option selected="selected" value="1">แสดง</option> 
						<option value="0">ไม่แสดง</option> 
					</select>
				</div> 
				<div class="form-group">
					<button name="save_addslide" type="submit" name="post_name" class="btn btn-success">Add Slide</button>
				</div> 
			</form>
		</div>
	</div>

<script type="text/javascript">
function del_slide(id)
{
if (confirm("คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้") == true) {
var xmlhttp;
if (window.XMLHttpRequest){
  xmlhttp=new XMLHttpRequest();
  }else{
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function(){
  if (xmlhttp.readyState==4 && xmlhttp.status==200){
    document.getElementById("showjs").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","./index.html?d=_lnwphp_banner_&t=id&i=" + id,true);
xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
xmlhttp.send();

document.getElementById("slide_"+id).remove();
    }
}
</script>

</div>