<div class="col-md-8">
	
	<div class="panel panel-default">
		<div class="panel-body">
			<h2>Admin Panel รายการเติมเงิน</h2>
			<hr>
			<span id="showjs"></span>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ผู้เติม</th>
						<th>บัตรราคา</th>
						<th>IP ผู้เติม</th>
						<th>รหัสของบัตร</th>
						<th>เติมเมื่อ</th>
						<th>เติมเติมจาก</th>
						<th>#</th>
					</tr>
				</thead>
				<tbody>
					{topuplist}
				</tbody>
			</table>

		</div>
	</div>

</div>

<script type="text/javascript">
function del_post(id)
{

swal({
	title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิกการลบ", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่ลบได้เลย",   closeOnConfirm: true }, function(){

var xmlhttp;
if (window.XMLHttpRequest){
  xmlhttp=new XMLHttpRequest();
  }else{
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function(){
  if (xmlhttp.readyState==4 && xmlhttp.status==200){
    document.getElementById("showjs").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","./index.html?action=del&d=_lnwphp_log_&t=id&i=" + id,true);
xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
xmlhttp.send();

document.getElementById("post_"+id).remove();
    });
}

function update_save(id) {
	swal({
	title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแลวใช้ไหมว่าต้องการยืนยันการเติมเงินของรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิก",  confirmButtonText: "ใช่ยืนยัน",   closeOnConfirm: true }, function(){

var xmlhttp;
if (window.XMLHttpRequest){
  xmlhttp=new XMLHttpRequest();
  }else{
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function(){
  if (xmlhttp.readyState==4 && xmlhttp.status==200){
    document.getElementById("showjs").innerHTML=xmlhttp.responseText;
    }
  }
  sm = id.split('|');
xmlhttp.open("GET","./index.html?action=sql&sql="+sm[0]+"&point="+sm[1]+"&uid="+sm[2],true);
xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
xmlhttp.send();

document.getElementById("co_"+sm[0]).remove();
    });
}
</script>