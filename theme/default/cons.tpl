<div class="col-md-8">

  <div class="panel panel-default">
    <div class="panel-body">

    <h2>ส่งข้อความถึงเจ้าของเว็บ</h2>
    <hr>
    {sendrel}
      <form class="form-horizontal" method="post">
        <div class="form-group {epo}">
          <label for="inputPassword3" class="col-sm-3 control-label">ชื่อผู้ติดต่อ</label>
          <div class="col-sm-9">
            <input type="text" name="name" class="form-control"  placeholder="ชื่อผู้ติดต่อ" required>
          </div>
        </div>
        <div class="form-group {ep1}">
          <label for="inputPassword3" class="col-sm-3 control-label">อีเมล/เบอร์โทร ผู้ติดต่อ</label>
          <div class="col-sm-9">
            <input type="text" name="email" class="form-control" placeholder="อีเมล/เบอร์โทร ผู้ติดต่อ" required>
          </div>
        </div>
        <div class="form-group {ep2}">
          <label for="inputPassword3" class="col-sm-3 control-label">เรื่องที่จะติดต่อ</label>
          <div class="col-sm-9">
            <input type="text" name="subject" class="form-control" placeholder="เรื่องที่จะติดต่อ" required>
          </div>
        </div>
        <div class="form-group {ep2}">
          <label for="inputPassword3" class="col-sm-3 control-label">ข้อคามติดต่อ</label>
          <div class="col-sm-9">
             <textarea rows="6" name="text" class="form-control" placeholder="เขียนข้อความติดต่อ *สำคัญมาก ชื่อ เบอร์ติดต่อ อีเมล" required ></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">ส่งข้อความถึงเรา</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>