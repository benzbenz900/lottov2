<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{title}</title>
  <link href="theme/default/component/css/bootstrap.min.css" rel="stylesheet">
  <link href="theme/default/component/css/todc-bootstrap.min.css" rel="stylesheet">
  <script src="theme/default/component/js/jquery.min.js"></script>
  <script src="theme/default/component/js/bootstrap.min.js"></script>
  <script src="theme/default/component/js/sweetalert.min.js"></script>
  <link href="theme/default/component/css/sweetalert.css" rel="stylesheet" media="screen">
  {headcode}

</head>
<body>

  <div class="container">
    <img src="upload/{image_header}" class="img-responsive" alt="{name_image_header}">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./"><span class="glyphicon glyphicon-home"></span> {home}</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            {show_menu}
          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li><a><span class="glyphicon glyphicon-calendar"></span> <span id="txt"></span> {name_user_id_point}<span class="label label-success">{user_id_point}</span></a></li>
          </ul>

        </div>
      </div>
    </nav>
  </div>

<script type="text/javascript">
function date_time(id)
{
        date = new Date;
        year = date.getFullYear();
        month = date.getMonth();
        months = new Array('มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม');
        d = date.getDate();
        day = date.getDay();
        days = new Array('วันอาทิตย์', 'วันจันทร์', 'วันอังคาร', 'วันพุธ', 'วันพฤหัสบดี', 'วันศุกร์', 'วันเสาร์');
        h = date.getHours();
        if(h<10)
        {
                h = "0"+h;
        }
        m = date.getMinutes();
        if(m<10)
        {
                m = "0"+m;
        }
        s = date.getSeconds();
        if(s<10)
        {
                s = "0"+s;
        }
        result = ''+days[day]+' '+d+' '+months[month]+' '+year+' '+h+':'+m+':'+s;
        document.getElementById(id).innerHTML = result;
        setTimeout('date_time("'+id+'");','1000');
        return true;
}
window.onload = date_time('txt');
</script>

  <div class="container">
    <div class="row">