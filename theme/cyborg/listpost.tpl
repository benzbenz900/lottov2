<div class="col-md-8">
	
	<div class="panel panel-default">
		<div class="panel-body">
			<h2>Admin Panel รายการโพสต์ต่างๆ</h2>
			<hr>
			<span id="showjs"></span>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>หัวเรื่อง</th>
						<th>ประเภท</th>
						<th>วิว/ดู</th>
						<th>โพสต์เมื่อ</th>
						<th>แก้ไข/ลบ</th>
					</tr>
				</thead>
				<tbody>
					{listpost}
				</tbody>
			</table>

		</div>
	</div>

<script type="text/javascript">
function del_post(id)
{
if (confirm("คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้") == true) {
var xmlhttp;
if (window.XMLHttpRequest){
  xmlhttp=new XMLHttpRequest();
  }else{
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function(){
  if (xmlhttp.readyState==4 && xmlhttp.status==200){
    document.getElementById("showjs").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","./index.html?d=ecGFnZV9l&t=taWQ=&i=" + id,true);
xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
xmlhttp.send();

document.getElementById("post_"+id).remove();
    }
}
</script>

</div>