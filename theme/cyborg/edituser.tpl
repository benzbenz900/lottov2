<div class="col-md-8">
  <div class="panel panel-default">
    <div class="panel-body">
      <h2>User แก้ไขผู้ใช่ <strong>{loginname}</strong></h2>
<hr>
{showalert}
      <form class="form-horizontal" method="post">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
          <div class="col-sm-10">
            <input type="email" name="email" class="form-control" value="{email}" placeholder="Email">
          </div>
        </div>
        <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">Point</label>
          <div class="col-sm-10">
            <input type="text" name="point" class="form-control" value="{point}" placeholder="Point">
          </div>
        </div>
        <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">Status</label>
          <div class="col-sm-10">
            <select name="status" class="form-control">
              <option {status}</option>
              <option value="0">User</option>
              <option value="2">Block</option>
              <option value="1">Admin</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="save_user" class="btn btn-success">Save</button>
          </div>
        </div>
      </form>

<h2>เลขที่ User <strong>{loginname}</strong> ชื้อ</h2>
<table class="table table-striped">
        <thead>
          <tr>
            <th>รายการ</th>
            <th>เลขที่เล่น</th>
            <th>อัตตราเล่น</th>
            <th>ประจำวันที่</th>
            <th>วันที่เวลาเล่น</th>
            <th>ลบรายการ</th>
          </tr>
        </thead>
        <tbody>
          {buy_menu}
        </tbody>
      </table>

      <h2>รายการเติมเงินของ User <strong>{loginname}</strong></h2>
<table class="table table-striped">
        <thead>
          <tr>
            <th>ผู้เติม</th>
            <th>บัตรราคา</th>
            <th>IP ผู้เติม</th>
            <th>รหัสของบัตร</th>
            <th>เติมเมื่อ</th>
            <th>เติมเติมจาก</th>
            <th>#</th>
          </tr>
        </thead>
        <tbody>
          {topuplist}
        </tbody>
      </table>

      <h2>รายการแจ้งถอนเงินจากเว็บของ User <strong>{loginname}</strong></h2>
              <table class="table table-striped">
                <thead>
                  <tr>
                  <th>ชื่อบัญชี</th>
                    <th>วันที่</th>
                    <th>ธนาคาร</th>
                    <th>จำนวน</th>
                    <th>สถานะ</th>
                    <th>สถานะ</th>
                  </tr>
                </thead>
                <tbody id="list_out_money">
                  {saveoutmoney}
                </tbody>
              </table>


    </div>
  </div>
</div>

<script type="text/javascript">
  function del_post(id){
    swal({
      title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิกการลบ", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่ลบได้เลย",   closeOnConfirm: true }, function(){

        var xmlhttp;
        if (window.XMLHttpRequest){
          xmlhttp=new XMLHttpRequest();
        }else{
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
          if (xmlhttp.readyState==4 && xmlhttp.status==200){
            document.getElementById('showjs').innerHTML = xmlhttp.responseText;
          }
        }
        xmlhttp.open("GET","./index.html?action=dZGVs&d=sc2F2ZV9udW1iX2k=&t=daWQ=&i=" + id,true);
        xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xmlhttp.send();
        document.getElementById("post_"+id).remove();
      });
  }

  function del_postt(id)
{

swal({
  title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิกการลบ", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่ลบได้เลย",   closeOnConfirm: true }, function(){

var xmlhttp;
if (window.XMLHttpRequest){
  xmlhttp=new XMLHttpRequest();
  }else{
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function(){
  if (xmlhttp.readyState==4 && xmlhttp.status==200){
    document.getElementById("showjs").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","./index.html?action=aZGVs&d=ibG9nX3o=&t=id&i=" + id,true);
xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
xmlhttp.send();

document.getElementById("post_"+id).remove();
    });
}

function update_save(id) {
  swal({
  title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแลวใช้ไหมว่าต้องการยืนยันการเติมเงินของรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิก",  confirmButtonText: "ใช่ยืนยัน",   closeOnConfirm: true }, function(){

var xmlhttp;
if (window.XMLHttpRequest){
  xmlhttp=new XMLHttpRequest();
  }else{
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function(){
  if (xmlhttp.readyState==4 && xmlhttp.status==200){
    document.getElementById("showjs").innerHTML=xmlhttp.responseText;
    }
  }
  sm = id.split('|');
xmlhttp.open("GET","./index.html?action=sc3Fs&sql="+sm[0]+"&point="+sm[1]+"&uid="+sm[2],true);
xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
xmlhttp.send();

document.getElementById("co_"+sm[0]).remove();
    });
}
function del_postw(id){
    swal({
      title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิกการลบ", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่ลบได้เลย",   closeOnConfirm: true }, function(){

        var xmlhttp;
        if (window.XMLHttpRequest){
          xmlhttp=new XMLHttpRequest();
        }else{
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
          if (xmlhttp.readyState==4 && xmlhttp.status==200){
            sm = xmlhttp.responseText;
            sm = sm.split('|');
            document.getElementById('money_point_my_you').innerHTML = sm[0];
            document.getElementById('showjs').innerHTML = sm[1];
          }
        }
        xmlhttp.open("GET","./index.html?action=sZGVs&d=uc2F2ZV93aXRoZHJhd2FsX3A=&t=WaWQ=&i=" + id,true);
        xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xmlhttp.send();
        id = id.split('|');
        document.getElementById("post_"+id[0]).remove();
      });
}
</script>