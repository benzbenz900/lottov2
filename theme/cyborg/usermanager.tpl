<div class="col-md-8">
  <div class="panel panel-default">
    <div class="panel-body">
    <h2>User ทั้งหมดในเว็บ</h2>
          <table class="table table-striped">
            <thead>
              <tr>
              <th>#id</th>
                <th>Login Name</th>
                <th>Email</th>
                <th>Join Date</th>
                <th>เงินในเว็บ</th>
                <th>สถานะ</th>
                <th>แก้ไข/ลบ</th>
              </tr>
            </thead>
            <tbody>
              {showuser}
            </tbody>
          </table>

          <center>{paginate}</center>

    </div>
  </div>
</div>

<script type="text/javascript">
  function del_post(id){
    swal({
      title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิกการลบ", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่ลบได้เลย",   closeOnConfirm: true }, function(){

        var xmlhttp;
        if (window.XMLHttpRequest){
          xmlhttp=new XMLHttpRequest();
        }else{
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
          if (xmlhttp.readyState==4 && xmlhttp.status==200){
            document.getElementById('showjs').innerHTML = xmlhttp.responseText;
          }
        }
        xmlhttp.open("GET","./index.html?action=oZGVs&d=tYWNjb3VudHNfdA==&t=caWQ=&i=" + id,true);
        xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xmlhttp.send();
        document.getElementById("post_"+id).remove();
      });
  }
</script>