<div class="col-md-8">
  <div class="panel panel-default">
    <div class="panel-body">
      <h2>User แก้ไขผู้ใช่ <strong>{loginname}</strong></h2>
<hr>
{showalert}
      <form class="form-horizontal" method="post">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">ชื่อที่แสดง</label>
          <div class="col-sm-10">
            <input type="text" name="vipname" class="form-control" value="{vipname}" placeholder="ชื่อที่แสดง">
          </div>
        </div>
        <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">ราคาบัตร/บาท</label>
          <div class="col-sm-10">
            <input type="number" name="vipprice" class="form-control" value="{vipprice}" placeholder="ราคาบัตร/บาท">
          </div>
        </div>
        <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">จำนวนที่จะได้ในเว็บ</label>
          <div class="col-sm-10">
            <input type="number" name="point" class="form-control" value="{point}" placeholder="จำนวนที่จะได้ในเว็บ">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="save_user" class="btn btn-success">Save</button>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>