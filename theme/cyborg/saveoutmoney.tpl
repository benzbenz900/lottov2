<div class="col-md-8">

  <div class="panel panel-default">
    <div class="panel-body">
      <h2>รายการแจ้งถอนเงิน Withdrawal</h2>
      <hr>
      <span>{showalert}</span>
      <div class="row">
        <div class="tab-content">
          <div class="tab-pane fade in active" id="topup">
            <div class="col-md-12">
              <span id="showjs"></span>
            </div>
            <div class="col-md-12">
              <h3>รายการแจ้งถอนเงินจากเว็บ</h3>
              <table class="table table-striped">
                <thead>
                  <tr>
                  <th>ชื่อผู้ใช่</th>
                  <th>ชื่อบัญชี</th>
                    <th>วันที่</th>
                    <th>ธนาคาร</th>
                    <th>จำนวน</th>
                    <th>สถานะ</th>
                    <th>ยืนยัน/ลบ</th>
                  </tr>
                </thead>
                <tbody id="list_out_money">
                  {saveoutmoney}
                </tbody>
              </table>

            </div>
          </div>

        </div>
      </div>
    </div>

  </div>
</div>

</div>

<script type="text/javascript">
  function se_null () {
    x = document.getElementById('se_l').value;
    d = document.getElementById('amount').value;
    user = document.getElementById('user').value;
    email = document.getElementById('email').value;
    if (x == "") {
      swal({   title: "บัญชีที่โอนเข้า!",   text: "กรุณาเลือก ธนาคารที่ทำการโอนด้วย",   type: "error",   confirmButtonText: "OK" });
    }else if(d == ""){
      swal({   title: "จำนวนเงินที่โอน!",   text: "กรุณาใส่จำนวนเงิน ที่โอน ก่อน",   type: "error",   confirmButtonText: "OK" });
    }else{
      var xmlhttp;
      if (window.XMLHttpRequest){
        xmlhttp=new XMLHttpRequest();
      }else{
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState==4 && xmlhttp.status==200){
          sm = xmlhttp.responseText;
          sm = sm.split('|');
          if (sm[0] == "1") {
            swal("สำเร็จ!", "บันทึกข้อมูลการแจ้งถอนเงินเรียบร้อยแล้ว", "success");
            document.getElementById('money_point_my_you').innerHTML = sm[1];
            document.getElementById('se_l').value = "";
            document.getElementById('amount').value = "";
            window.load_table();
          }else if(sm[0] == "2"){
            swal({   title: "ขออภัย!",   text: "ขออภัย การบันทึกไม่สามารถทำได้ กรุณาลองอีกครั้ง",   type: "error",confirmButtonText: "OK" });
          }else if(sm[0] == "3"){
            swal({   title: "ขออภัย!",   text: "ขออภัย ยอดเงินในบัญชีของคุณน้อยกว่า "+d+" ไม่สามารถถอนเงินตามจำนวนนี้ได้",   type: "error",confirmButtonText: "OK" });
          };
        }
      }
      xmlhttp.open("GET","./index.html?action=tc2F2ZV9vdXRfbW9uZXk=&uid={user_id}&u="+user+"&e="+email+"&p="+d+"&t={time_date_now}&b="+x+"&i={ip_user}",true);
      xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xmlhttp.send();
    };
  }

  function del_post(id){
    swal({
      title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิกการลบ", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่ลบได้เลย",   closeOnConfirm: true }, function(){

        var xmlhttp;
        if (window.XMLHttpRequest){
          xmlhttp=new XMLHttpRequest();
        }else{
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
          if (xmlhttp.readyState==4 && xmlhttp.status==200){
            sm = xmlhttp.responseText;
            sm = sm.split('|');
            document.getElementById('money_point_my_you').innerHTML = sm[0];
            document.getElementById('showjs').innerHTML = sm[1];
          }
        }
        xmlhttp.open("GET","./index.html?action=rZGVs&d=pc2F2ZV93aXRoZHJhd2FsX3A=&t=uaWQ=&i=" + id,true);
        xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xmlhttp.send();
        id = id.split('|');
        document.getElementById("post_"+id[0]).remove();
      });
}

function load_table () {
  var xmlhttp;
  if (window.XMLHttpRequest){
    xmlhttp=new XMLHttpRequest();
  }else{
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function(){
    if (xmlhttp.readyState==4 && xmlhttp.status==200){
      document.getElementById('list_out_money').innerHTML = xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","./index.html?action=lbG9hZF90YWJsZV93aWRhdw==",true);
  xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xmlhttp.send();
}
</script>