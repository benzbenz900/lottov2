<div class="col-md-8">
	
	<div class="panel panel-default">
		<div class="panel-body">
			<h2>Admin Panel</h2>
			<div class="row">
			<div class="col-md-6">
				<div class="list-group">
					{admin_menu_1}
				</div>
			</div>
			<div class="col-md-6">
				<div class="list-group">
					{admin_menu_2}
				</div>
			</div>
			</div>
			{admin_alert}
			<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
			<form method="post">
				<div class="form-group">
					<label>ข้อความประกาศหน้าแรก</label>
					<textarea name="post_alert" class="form-control" placeholder="ข้อความประกาศหน้าแรก">{post_alert}</textarea>
				</div>
				<button name="save_admin" type="submit" class="btn btn-success">บันทึกข้อความประกาศ</button>
			</form>
			<script>
				CKEDITOR.replace('post_alert');
			</script>
		</div>
	</div>

</div>