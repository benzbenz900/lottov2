<div class="col-md-8">
	<div class="panel panel-default">
		<div class="panel-body">
			<h2>TopUP Money</h2>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-danger">
						<strong>คำเตือน</strong> กรุณาอ่าน <a href="#terms" data-toggle="tab"><b>ข้อตกลง</b></a> ในการเติมเงินของเรา
					</div>
				</div>
				<div class="tab-content">
					<div class="tab-pane fade in active" id="topup">

						<div class="col-md-6">
							<center><h3>รายละเอียดแต้มที่จะได้รับ</h3></center>

							<table class="table">
								<thead>
									<tr class="success">
										<th>ราคาบัตร</th>
										<th>แต้มที่ได้รับ</th>
									</tr>
								</thead>
								<tbody>
									{show_vip}
								</tbody>
							</table>
							<a class="btn btn-danger btn-block" href="./topuptm.html">เติมเงิน</a>

						</div>
						<div class="col-md-6">
							<center><h3>กรอกรหัสบัตร</h3></center>
							<div class="form-group">
								<label>เลข 14 หลัก</label>
								<input class="form-control" name="tmn_password" type="text" id="tmn_password" maxlength="14" placeholder="เลข 14 หลัก" required>
							</div>
							<div class="form-group">
								<label>Login Name</label>
								<input class="form-control" name="ref1" type="text" id="ref1" placeholder="Login Name" value="{login_name}" required>
							</div>
							<div class="form-group">
								<label>Email</label>
								<input class="form-control" name="ref2" type="text" id="ref2" placeholder="Email" value="{email}" required>
							</div>
							<div class="form-group">
								<label>หมายเหตุอื่นๆ</label>
								<input class="form-control" name="ref3" type="text" id="ref3" value="-" placeholder="หมายเหตุอื่นๆ">
							</div>
							<a class="btn btn-info" href="./topuptm.html#terms" data-toggle="tab">ข้อตกลง</a> <button type="submit" class="btn btn-large btn-success" onclick="submit_tmnc()" >เติมเงิน</button>
						</div>
					</div>
					<div class="tab-pane fade in" id="terms">
						<div class="col-md-12">
							{terms}
							<hr>
							<a class="btn btn-success" href="./topuptm.html#topup" data-toggle="tab">ยอมับเงื่อนไข</a> <a class="btn btn-danger" href="./index.html">ไม่ยอมรับเงื่อนไข</a>
							<hr>
						</div>
					</div>


				</div>

				<div class="col-md-12">
				<hr>
					<center>
						<div class="alert alert-danger">
							<strong>กรุณาอ่านเงื่อนไขต่างๆก่อนการเติมเงินทุกครั้ง</strong> หากเติมเงินเข้ามาแล้ว เราถือว่าท่านยอมรับเงื่อนไขทุกประการ
						</div>
					</center>
				</div>
			</div>

		</div>
	</div>

	<script type="text/javascript" src='https://www.tmtopup.com/topup/3rdTopup.php?uid={tmtopup_uid}'></script>
</div>