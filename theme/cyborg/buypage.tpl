<div class="col-md-8">
	<div class="panel panel-default">
		<div class="panel-body">
			<h2>{buypage}</h2>
			<div class="row">
				<div class="col-md-12">
					<table class="table">
					<caption>เลือกหมวดเลขที่จะแทง</caption>
						<thead>
							<tr>
								<th>หมวดเลข</th>
								<th>ประจำงวด</th>
								<th>หมดเวลาแทง</th>
								<th>บาทละ</th>
								<th>เข้าแทง</th>
							</tr>
						</thead>
						<tbody>
							{buy_menu_1}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-body">
			<div class="col-md-8"><h2>{buynumber}</h2></div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>รายการ</th>
								<th>เลขที่เล่น</th>
								<th>อัตตราเล่น</th>
								<th>ประจำวันที่</th>
								<th>วันที่เวลาเล่น</th>
								<th>ลบรายการ</th>
							</tr>
						</thead>
						<tbody>
							{buy_menu_2}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>

<script type="text/javascript">
	function del_post(id){
		swal({
			title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการลบรายการนี้",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิกการลบ", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่ลบได้เลย",   closeOnConfirm: true }, function(){

				var xmlhttp;
				if (window.XMLHttpRequest){
					xmlhttp=new XMLHttpRequest();
				}else{
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function(){
					if (xmlhttp.readyState==4 && xmlhttp.status==200){
						ids = xmlhttp.responseText.split('|');
						if (ids[1] == '1') {
							document.getElementById("post_"+id).remove();
						}else if (ids[1] == '2') {
							swal({   title: "ไม่สามารถยกเลิกได้!",   text: "ขออภัย ไม่สามารถยกเลิกได้ เนื่องจากเกินเวลาที่กำหนด "+ids[2]+" นาที ไปแล้ว ไม่สามารถยกเลิกได้",   type: "error",confirmButtonText: "OK" });
						}else{
							swal({   title: "ขออภัย!",   text: "เกินขอผิดพลาดที่ไม่ทราบสาเหตุ ("+xmlhttp.responseText+")",   type: "error",confirmButtonText: "OK" });
						}
					}
				}
				xmlhttp.open("GET","./index.html?action=dZGVs&d=tc2F2ZV9udW1iX2k=&t=gaWQ=&i=" + id,true);
				xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
				xmlhttp.send();
			});
}
function success (uid, sid, nid) {
	swal({
		title: "แน่ใจแล้วใช่ไหม",   text: "คุณแน่ใจแล้วใช้ไหมว่าต้องการปรับเป็นถูกรางวัล",   type: "warning",   showCancelButton: true, cancelButtonText: "ยกเลิก", confirmButtonColor: "#DD6B55",   confirmButtonText: "ใช่",   closeOnConfirm: true }, function(){

			var xmlhttp;
			if (window.XMLHttpRequest){
				xmlhttp=new XMLHttpRequest();
			}else{
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function(){
				if (xmlhttp.readyState==4 && xmlhttp.status==200){
					if (xmlhttp.responseText == 1) {
						if (sid == 3) {
							xid = '<span class="label label-default">รับเงินแล้ว</span>';
						}else{
							xid = '<span class="label label-info">ไม่ทราบผล</span>';
						};
						document.getElementById('post_'+nid).innerHTML = xid;
					};
				}else{
					document.getElementById('post_'+nid).innerHTML = xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET","./index.html?action=uc2F2ZV9jaGVjaw==&uid="+uid+"&sid="+sid+"&nid="+nid,true);
			xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
			xmlhttp.send();
		});
}
</script>