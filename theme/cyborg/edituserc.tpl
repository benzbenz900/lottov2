<div class="col-md-8">
  <div class="panel panel-default">
    <div class="panel-body">
      <h2>แก้ไขผู้ใช่ <strong>{loginname}</strong></h2>
<hr>
{showalert}
      <form class="form-horizontal" method="post">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
          <div class="col-sm-10">
            <input type="email" name="email" class="form-control" value="{email}" placeholder="Email" disabled="">
          </div>
        </div>
        <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">Login Name</label>
          <div class="col-sm-10">
            <input type="text" name="name" class="form-control" value="{name}" placeholder="Login Name" disabled="">
          </div>
        </div>
        <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">Phone</label>
          <div class="col-sm-10">
            <input type="text" name="phone" class="form-control" value="{phone}" placeholder="เบอร์มือถือ">
          </div>
        </div>
        <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">ID ผู้แนะนำ</label>
          <div class="col-sm-10">
            <input type="text" name="ref" class="form-control" value="{ref}" placeholder="เปลี่ยนผู้แนะนำ">
            <code>ไม่เอาผู้แนะนำใส่ 0</code>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="save_user" class="btn btn-success">แก้ไข</button>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>