<div class="col-md-8">
	<div class="panel panel-default">
		<div class="panel-body">
			<h2>Admin Panel Setting Website</h2>
			<hr>
			{show_alert}
			<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<form method="post" action="./cmd@setting.admin" enctype="multipart/form-data">
	<div class="form-group">
		<label>ชื่อเว็บ</label>
		<input type="text" name="webname" class="form-control" placeholder="ชื่อเว็บ" value="{webname}">
	</div>
	<div class="form-group">
		<label>ข้อความท้ายเว็บ <code>ใช่ HTML ได้</code></label>
		<textarea name="foot_text" class="form-control" placeholder="ข้อความท้ายเว็บ">{foot_text}</textarea>
	</div>
	<div class="form-group">
		<label>ธีมของเว็บ</label>
		<select name="theme" class="form-control">
			{list_theme}
		</select>
	</div>
	<div class="form-group">
		<label>URL FanPage Facebook <code>เช่น https://www.facebook.com/intervpshosting</code></label>
		<input type="text" name="fan_page" class="form-control" placeholder="URL FanPage Facebook" value="{fan_page}">
	</div>
	<div class="form-group">
		<label>รูปหัวเว็บ</label>
		<input type="file" name="image_header">
		<img src="upload/{image_header}" class="img-responsive" alt="{name_image_header}">
	</div>
	<div class="form-group">
		<label>คำอธิบายรูปหัวเว็บ</label>
		<input type="text" name="name_image_header" class="form-control" placeholder="ใส่คำอะธิบายรูปหัวเว็บเพื่อเป็น seo" value="{name_image_header}">
	</div>
	<div class="form-group">
		<label>เปิด ปิดการแสดงสไลต์โชว์ในหน้าแรก</label>
		<select name="banner_show" class="form-control">
			<option value="1">เปิด</option>
			<option value="0">ปิด</option>
		</select>
	</div>
	<div class="form-group">
		<label>แทรก HTML Code นับสถิติ</label>
		<textarea name="code_stat" class="form-control" placeholder="แทรก HTML Code นับสถิติ">{code_stat}</textarea>
	</div>
	<div class="form-group">
		<label>TmTopUP UID</label>
		<input type="text" name="tmtopup_uid" class="form-control" placeholder="ใส่ UID ของ Tmtopup.com" value="{tmtopup_uid}">
	</div>
	<div class="form-group">
		<label>TmTopUP Pass Key</label>
		<input type="text" name="tmtopup_passkey" class="form-control" placeholder="ใส่ Pass Key ที่ตั้งเองในเว็บ Tmtopup.com" value="{tmtopup_passkey}">
	</div>
	<div class="form-group">
		<label>TmTopUP URL API</label>
		<input type="url" disabled='disabled' class="form-control" value="{tmtopup_url_api}">
	</div>
	<div class="form-group">
		<label>แทรก HTML Code หัวเวบ <code>เช่น meta tag ต่างๆ</code></label>
		<textarea name="headcode" class="form-control" placeholder="แทรก HTML Code หัวเวบ">{headcode}</textarea>
	</div>
	<div class="form-group">
		<label>แทรก HTML Code ท้ายเว็บ <code>เช่น script ต่างๆ</code></label>
		<textarea name="footcode" class="form-control" placeholder="แทรก HTML Code ท้ายเว็บ">{footcode}</textarea>
	</div>
	<div class="form-group">
		<label>% ที่จะได้รับเมื่อมีการ ชื้อเลข</label>
		<input type="text" name="percent" class="form-control" value="{percent}">
	</div>
	<div class="form-group">
		<label>รับค่าคอมได้ทุกๆวันที่ เช่น 1,16</label>
		<input type="text" name="commissions_day" class="form-control" value="{commissions_day}">
	</div>
	<div class="form-group">
		<label>เวลาในการยกเลิก โพยเลข ที่ลูกค้าแทง <code>กำหนดเวลาในการยกเลิก เช่น 30 นาทีหลังจากชื้อเลข ให้ใส่เป็น 30 หาก 1 วัน ใส่เป็น 1440 (1h = 60m)</code></label>
		<input type="text" name="cancel" class="form-control" value="{cancel}">
	</div>
	<div class="form-group">
		<label>เขียนข้อตกลงในการเติมเงิน เติมเกมส์ของคุณ</label>
		<textarea name="terms" class="form-control" placeholder="เขียนข้อตกลงในการเติมเงิน เติมเกมส์ของคุณ">{terms}</textarea>
	</div>
	<button name="save_setting" type="submit" class="btn btn-success">บันทึกการตั้งค่า</button>
</form>
<script>
	CKEDITOR.replace('terms');
</script>
		</div>
	</div>

</div>