<?php
if (!defined("isdoc")){header('HTTP/1.1 404 Not Found');echo "Not Link Direct File"; exit;}

$_rt = new __rtag;
$_c = new _condb;
$_f = new __form;
$_sl = new __repl;


$_sq = "SELECT * FROM `_lnwphp_page_` WHERE `show_` = '1' AND `type` = '0'   ORDER BY idorder";
$_qr = mysqli_query($_c->d(),$_sq);
$_mu = '<a class="list-group-item '.(!isset($_GET['page']) ? "active" : "").'" href="./"><span class="glyphicon glyphicon-home"></span> '._home_.'</a>
<a class="list-group-item '.(isset($_GET['page']) && $_GET['page'] == 'rates' ? "active" : "").'" href="./rates.html"><span class="glyphicon glyphicon-th"></span> '._rates.'</a>
<a class="list-group-item '.(isset($_GET['page']) && $_GET['page'] == 'cons' ? "active" : "").'" href="./cons.html"><span class="glyphicon glyphicon-th"></span> '._cons.'</a>
<a class="list-group-item '.(isset($_GET['page']) && $_GET['page'] == 'event' ? "active" : "").'" href="./event.html"><span class="glyphicon glyphicon-th"></span> '._event_news_.'</a>';
while ($_rr = mysqli_fetch_object($_qr)) {
	if ($_rr->url_ != "") {
		$_mu .= '<a class="list-group-item" target="_blank" href="'.$_rr->url_.'"><span class="glyphicon glyphicon-link"></span> '.$_rr->name.'</a>';
	} else {
		$_mu .= '<a class="list-group-item '.(isset($_GET['page']) && $_GET['page'] == 'page' && isset($_GET['pid']) && $_GET['pid'] == $_rr->id ? "active" : "").'" href="'._urlconfig_.$_rr->id.'-'.$_rt->ps($_rr->name).'.htm"><span class="glyphicon glyphicon-th-large"></span> '.$_rr->name.'</a>';
	}
}

$_s = $_f->__form_("menu");
$_s = $_sl->__repl_($_s,"{fanpage}",_fanpage_);

if (isset($_SESSION["login_name"]) OR isset($_COOKIE['login_name'])) {
	if (__isadmin__ == "1") {
		$_mu .= '<a class="list-group-item '.(isset($_GET['page']) && $_GET['page'] == 'admin' ? "active" : "").'" href="./admin.html"><span class="glyphicon glyphicon-th"></span> '._admin_manager_.'</a>';
	}
	$_mu .= '<a class="list-group-item '.(isset($_GET['page']) && $_GET['page'] == 'buypage' ? "active" : "").'" href="./buypage.html"><span class="glyphicon glyphicon-th"></span> '._buypage_.'</a>
	<a class="list-group-item '.(isset($_GET['page']) && $_GET['page'] == 'userpage' ? "active" : "").'" href="./userpage.html"><span class="glyphicon glyphicon-user"></span> '._manager_.'</a>
	<a class="list-group-item '.(isset($_GET['page']) && $_GET['page'] == 'changepass' ? "active" : "").'" href="./changepass.html"><span class="glyphicon glyphicon-user"></span> '._chang_pass_.'</a>
	<a class="list-group-item '.(isset($_GET['page']) && $_GET['page'] == 'withdrawal' ? "active" : "").'" href="./withdrawal.html"><span class="glyphicon glyphicon-th"></span> '._widaw_.'</a>
	<a class="list-group-item '.(isset($_GET['page']) && $_GET['page'] == 'topuptm' ? "active" : "").'" href="./topuptm.html"><span class="glyphicon glyphicon-user"></span> '._top_up_.'</a>
	<a class="list-group-item '.(isset($_GET['page']) && $_GET['page'] == 'topupb' ? "active" : "").'" href="./topupb.html"><span class="glyphicon glyphicon-user"></span> '._top_up_bank_.'</a>
	<a class="list-group-item" href="./index.php?page=logout"><span class="glyphicon glyphicon-user"></span> '._logout_.'</a>';
}else{
	$_mu .= '<a class="list-group-item '.(isset($_GET['page']) && $_GET['page'] == 'register' ? "active" : "").'" href="./register.html"><span class="glyphicon glyphicon-user"></span> '._regiter_.'</a>
	<a class="list-group-item '.(isset($_GET['page']) && $_GET['page'] == 'resetpassword' ? "active" : "").'" href="./resetpassword.html"><span class="glyphicon glyphicon-user"></span> '._resetpassword_.'</a>
	<a class="list-group-item '.(isset($_GET['page']) && $_GET['page'] == 'login' ? "active" : "").'" href="./login.html"><span class="glyphicon glyphicon-log-in"></span> '._login_.'</a>';
}

$_s = $_sl->__repl_($_s,"{show_menu}",$_mu);

new __show($_s);
?>