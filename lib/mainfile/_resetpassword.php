<?php
if (!defined("isdoc")){
	header('HTTP/1.1 404 Not Found');
	echo "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n<html><head>\n<title>404 Not Found</title>\n</head>";
	echo "<body>\n<h1>Not Found</h1>\n<p>The requested URL ".$_SERVER['REQUEST_URI']." was not found on this server.</p>\n";
	echo "<hr>\n".$_SERVER['SERVER_SIGNATURE']."\n</body></html>\n";
	exit;
}
$_f = new __form;
$_s = $_f->__form_("resetpassword");
$ecs = new ecs;
if (isset($_POST['email'])) {
	$email = $ecs->e($_POST['email']);
	$_c = new _condb;
	$sqlchk = "SELECT * FROM `_lnwphp_accounts_`  WHERE `email`='$email'";
	$resultchk = mysqli_query($_c->d(),$sqlchk);
	$rowschk = mysqli_num_rows($resultchk);
	if ($rowschk > 0) {
		$Resulchk = mysqli_fetch_array($resultchk);
		$login_name = $Resulchk['login'];
		$lnwphp_random = new __random_str;
		$_lnwphp_password = new __encrypt;
		$_pass = $lnwphp_random->__random_str_("10");
		$password_new = $_lnwphp_password->_encrypt($_pass);
		mysqli_query($_c->d(),"UPDATE `_lnwphp_accounts_` SET `password` = '$password_new' WHERE `email` = '$email'");

		$_f = new __form;
		$_se = $_f->__form_("mail_resetpass");

		$_sl = new __repl;
		$_se = $_sl->__repl_($_se,"{new_password}",$_pass);
		$_se = $_sl->__repl_($_se,"{login_name}",$login_name);
		$_se = $_sl->__repl_($_se,"{email}",$email);

		new __send_mail($email,"รหัสผ่านใหม่ของ ".$Resulchk['login'],$_se);

		$_sa = '<div class="alert alert-dismissable alert-success">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>สำเร็จ</strong> ระบบได้ส่ง "รหัสผ่านใหม่" ไปยัง '.$email.' ของคุณแล้ว</div>';
	}else{
		$_sa = '<div class="alert alert-dismissable alert-warning">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>โอ้ๆๆๆ</strong> ไม่มีอีเมล์นี้ '.$email.' ในระบบ <a role="button" class="btn btn-success" href="./register.html">Register</a></div>';
	}
}else{
	$_sa = "";
}

$_sl = new __repl;
$_s = $_sl->__repl_($_s,"{show_alert}",$_sa);

new __show($_s);
?>