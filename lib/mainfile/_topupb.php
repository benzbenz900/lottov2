<?php
if (!defined("isdoc")){header('HTTP/1.1 404 Not Found');echo "Not Link Direct File"; exit;}

function get_client_ip() {
	$ipaddress = '';
	if($_SERVER['REMOTE_ADDR'])
		$ipaddress = $_SERVER['REMOTE_ADDR'];
	else
		$ipaddress = 'UNKNOWN';
	return $ipaddress;
}

$_c = new _condb;

if (isset($_SESSION["login_name"]) OR isset($_COOKIE['login_name'])) {

	if (isset($_SESSION["login_name"])) {
		$login_name = $_SESSION["login_name"];
	}elseif (isset($_COOKIE['login_name'])) {
		$login_name = $_COOKIE['login_name'];
	}

	$b_detail = "";
	$show_select = '';
	$bk = "SELECT * FROM `_lnwphp_bank_`";
	$bq = mysqli_query($_c->d(),$bk);
	while ($br = mysqli_fetch_object($bq)) {
		$b_detail .=
		_acc_name_.$br->acc_name.'<br>'.
		_acc_numb_.$br->acc_numb.'<br>'.
		_acc_type_.$br->acc_type.'<br>'.
		_acc_bank_.$br->acc_bank.'<hr>';
		$show_select .= '<option value="('.$br->acc_bank.')'.$br->acc_name.'">('.$br->acc_bank.')'.$br->acc_name.'</option>';
	}

	$sql_admin = "SELECT * FROM `_lnwphp_log_` WHERE email='".__email__."'";
	$Query_admin = mysqli_query($_c->d(),$sql_admin);
	$_admin_show_alert = "";
	while ($Result_admin = mysqli_fetch_object($Query_admin)) {
		$type = ($Result_admin->type == 0) ? "TmTopUp" : "Bank";
		if ($Result_admin->success == 0) {
			$retVal = '<button class="btn btn-xs btn-danger" onclick="del_post(\''.$Result_admin->id.'\')">ลบ</button>';
		}else{
			$retVal = '';
		}
		$_admin_show_alert .= '<tr id="post_'.$Result_admin->id.'">
		<td>'.$Result_admin->username.'</td>
		<td>'.$Result_admin->price.'</td>
		<td>'.$Result_admin->client_ip.'</td>
		<td>'.$Result_admin->card_password.'</td>
		<td>'.$Result_admin->time_log.'</td>
		<td>'.$type.'</td>
		<td>'.$retVal.'</td>
	</tr>';
}

$_lnwphp_in_th = new __form;
$_s = $_lnwphp_in_th->__form_("topupb");

$_sl = new __repl;
$_s = $_sl->__repl_($_s,"{show_bank_detail}",$b_detail);
$_s = $_sl->__repl_($_s,"{show_select}",$show_select);
$_s = $_sl->__repl_($_s,"{login_name}",($login_name != '' ? $login_name.'" disabled="' : ""));
$_s = $_sl->__repl_($_s,"{email}",(__email__ != '' ? __email__.'" disabled="' : ""));
$_s = $_sl->__repl_($_s,"{terms}",__terms__);
$_s = $_sl->__repl_($_s,"{time_date_now}",date("Y-m-d H:i"));
$_s = $_sl->__repl_($_s,"{ip_user}",get_client_ip());
$_s = $_sl->__repl_($_s,"{topuplist}",$_admin_show_alert);
new __show($_s);

}
?>