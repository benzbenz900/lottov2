<?php
if (!defined("isdoc")){header('HTTP/1.1 404 Not Found');echo "Not Link Direct File"; exit;}
$_c = new _condb;
$cutStr = new __cut_str;
$_lnwphp_remove_tag = new __rtag;
$news_ch1 = "SELECT * FROM `_lnwphp_page_` WHERE `show_` = '1' and `type` = '1' ORDER BY id desc limit 10";
$news_ch2 = mysqli_query($_c->d(),$news_ch1)or die(mysqli_error());
$news_show = "";
while ($news_ch = mysqli_fetch_array($news_ch2)) {
	if(strpos($news_ch['detail'], '.png') !== false) {
		$png = explode(".png", $news_ch['detail']);
		$png = explode("src=", $png['0']);
		$image_img = end($png).".png";
	}elseif(strpos($news_ch['detail'], '.jpg') !== false) {
		$jpg = explode(".jpg", $news_ch['detail']);
		$jpg = explode("src=", $jpg['0']);
		$image_img = end($jpg).".jpg";
	}else{
		$image_img = '"'._urlconfig_.'theme/'._themename_.'/component/images/news.png';
	}
	$news_show .= '<ul class="media-list">
	<li class="media">
		<div class="media-left">
			<a href="'._urlconfig_.''.$news_ch['id'].'-'.$_lnwphp_remove_tag->ps($news_ch['name']).'.htm">
				<img style="width: 64px;" class="media-object" src='.$image_img.'" alt="'.$news_ch['name'].'">
			</a>
		</div>
		<div class="media-body">
			<h4 class="media-heading"><a href="'._urlconfig_.''.$news_ch['id'].'-'.$_lnwphp_remove_tag->ps($news_ch['name']).'.htm">'.$news_ch['name'].'</a></h4>
			'.$cutStr->__cut_str_(strip_tags($news_ch['detail']),"120","...").'
		</div>
	</li>
</ul>
</div>
<div class="col-md-6">';
}

$promotion_1 = "SELECT * FROM `_lnwphp_page_` WHERE `show_` = '1' and `type` = '2' ORDER BY id desc limit 10";
$promotion_2 = mysqli_query($_c->d(),$promotion_1)or die(mysqli_error());
$promotion_show = "";
while ($promotion_3 = mysqli_fetch_array($promotion_2)) {
	if(strpos($promotion_3['detail'], '.png') !== false) {
		$png = explode(".png", $promotion_3['detail']);
		$png = explode("src=", $png['0']);
		$image_img = end($png).".png";
	}elseif(strpos($promotion_3['detail'], '.jpg') !== false) {
		$jpg = explode(".jpg", $promotion_3['detail']);
		$jpg = explode("src=", $jpg['0']);
		$image_img = end($jpg).".jpg";
	}else{
		$image_img = '"'._urlconfig_.'theme/'._themename_.'/component/images/news.png';
	}
	$promotion_show .= '<ul class="media-list">
	<li class="media">
		<div class="media-left">
			<a href="'._urlconfig_.''.$promotion_3['id'].'-'.$_lnwphp_remove_tag->ps($promotion_3['name']).'.htm">
				<img style="width: 64px;" class="media-object" src='.$image_img.'" alt="'.$promotion_3['name'].'">
			</a>
		</div>
		<div class="media-body">
			<h4 class="media-heading"><a href="'._urlconfig_.''.$promotion_3['id'].'-'.$_lnwphp_remove_tag->ps($promotion_3['name']).'.htm">'.$promotion_3['name'].'</a></h4>
			'.$cutStr->__cut_str_(strip_tags($promotion_3['detail']),"120","...").'
		</div>
	</li>
</ul>
</div>
<div class="col-md-6">';
}
$events_1 = "SELECT * FROM `_lnwphp_page_` WHERE `show_` = '1' and `type` = '3' ORDER BY id desc limit 10";
$events_2 = mysqli_query($_c->d(),$events_1)or die(mysqli_error());
$events_show = "";
while ($events_3 = mysqli_fetch_array($events_2)) {
	if(strpos($events_3['detail'], '.png') !== false) {
		$png = explode(".png", $events_3['detail']);
		$png = explode("src=", $png['0']);
		$image_img = end($png).".png";
	}elseif(strpos($events_3['detail'], '.jpg') !== false) {
		$jpg = explode(".jpg", $events_3['detail']);
		$jpg = explode("src=", $jpg['0']);
		$image_img = end($jpg).".jpg";
	}else{
		$image_img = '"'._urlconfig_.'theme/'._themename_.'/component/images/news.png';
	}
	$events_show .= '<ul class="media-list">
	<li class="media">
		<div class="media-left">
			<a href="'._urlconfig_.''.$events_3['id'].'-'.$_lnwphp_remove_tag->ps($events_3['name']).'.htm">
				<img style="width: 64px;" class="media-object" src='.$image_img.'" alt="'.$events_3['name'].'">
			</a>
		</div>
		<div class="media-body">
			<h4 class="media-heading"><a href="'._urlconfig_.''.$events_3['id'].'-'.$_lnwphp_remove_tag->ps($events_3['name']).'.htm">'.$events_3['name'].'</a></h4>
			'.$cutStr->__cut_str_(strip_tags($events_3['detail']),"120","...").'
		</div>
	</li>
</ul>
</div>
<div class="col-md-6">';
}

$events_photo_1 = "SELECT * FROM `_lnwphp_page_` WHERE `show_` = '1' and `type` = '4' ORDER BY id desc limit 10";
$events_photo_2 = mysqli_query($_c->d(),$events_photo_1)or die(mysqli_error());
$events_photo_show = "";
while ($events_photo_3 = mysqli_fetch_array($events_photo_2)) {
	if(strpos($events_photo_3['detail'], '.png') !== false) {
		$png = explode(".png", $events_photo_3['detail']);
		$png = explode("src=", $png['0']);
		$image_img = end($png).".png";
	}elseif(strpos($events_photo_3['detail'], '.jpg') !== false) {
		$jpg = explode(".jpg", $events_photo_3['detail']);
		$jpg = explode("src=", $jpg['0']);
		$image_img = end($jpg).".jpg";
	}else{
		$image_img = '"'._urlconfig_.'theme/'._themename_.'/component/images/news.png';
	}
	$events_photo_show .= '<ul class="media-list">
	<li class="media">
		<div class="media-left">
			<a href="'._urlconfig_.''.$events_photo_3['id'].'-'.$_lnwphp_remove_tag->ps($events_photo_3['name']).'.htm">
				<img style="width: 64px;" class="media-object" src='.$image_img.'" alt="'.$events_photo_3['name'].'">
			</a>
		</div>
		<div class="media-body">
			<h4 class="media-heading"><a href="'._urlconfig_.''.$events_photo_3['id'].'-'.$_lnwphp_remove_tag->ps($events_photo_3['name']).'.htm">'.$events_photo_3['name'].'</a></h4>
			'.$cutStr->__cut_str_(strip_tags($events_photo_3['detail']),"120","...").'
		</div>
	</li>
</ul>
</div>
<div class="col-md-6">';
}

$screenshot_1 = "SELECT * FROM `_lnwphp_page_` WHERE `show_` = '1' and `type` = '5' ORDER BY id desc limit 10";
$screenshot_2 = mysqli_query($_c->d(),$screenshot_1)or die(mysqli_error());
$screenshot_show = "";
while ($screenshot_3 = mysqli_fetch_array($screenshot_2)) {
	if(strpos($screenshot_3['detail'], '.png') !== false) {
		$png = explode(".png", $screenshot_3['detail']);
		$png = explode("src=", $png['0']);
		$image_img = end($png).".png";
	}elseif(strpos($screenshot_3['detail'], '.jpg') !== false) {
		$jpg = explode(".jpg", $screenshot_3['detail']);
		$jpg = explode("src=", $jpg['0']);
		$image_img = end($jpg).".jpg";
	}else{
		$image_img = '"'._urlconfig_.'theme/'._themename_.'/component/images/news.png';
	}
	$screenshot_show .= '<ul class="media-list">
	<li class="media">
		<div class="media-left">
			<a href="'._urlconfig_.''.$screenshot_3['id'].'-'.$_lnwphp_remove_tag->ps($screenshot_3['name']).'.htm">
				<img style="width: 64px;" class="media-object" src='.$image_img.'" alt="'.$screenshot_3['name'].'">
			</a>
		</div>
		<div class="media-body">
			<h4 class="media-heading"><a href="'._urlconfig_.''.$screenshot_3['id'].'-'.$_lnwphp_remove_tag->ps($screenshot_3['name']).'.htm">'.$screenshot_3['name'].'</a></h4>
			'.$cutStr->__cut_str_(strip_tags($screenshot_3['detail']),"120","...").'
		</div>
	</li>
</ul>
</div>
<div class="col-md-6">';
}

$_lnwphp_in_th = new __form;
$_search = $_lnwphp_in_th->__form_("event");

$_lnwphp_re = new __repl;
$_tpl_show = $_lnwphp_re->__repl_($_search,"{text}",__post_alert__);
$_tpl_show = $_lnwphp_re->__repl_($_tpl_show,"{news_show}",($news_show != '' ? $news_show : "No Data"));
$_tpl_show = $_lnwphp_re->__repl_($_tpl_show,"{promotiom_show}",($promotion_show != '' ? $promotion_show : "No Data"));
$_tpl_show = $_lnwphp_re->__repl_($_tpl_show,"{events_show}",($events_show != '' ? $events_show : "No Data"));
$_tpl_show = $_lnwphp_re->__repl_($_tpl_show,"{events_photo_show}",($events_photo_show != '' ? $events_photo_show : "No Data"));
$_tpl_show = $_lnwphp_re->__repl_($_tpl_show,"{screenshot_show}",($screenshot_show != '' ? $screenshot_show : "No Data"));

new __show($_tpl_show);
?>