<?php
if (!defined("isdoc")){header('HTTP/1.1 404 Not Found');echo "Not Link Direct File"; exit;}

function get_client_ip() {
	$ipaddress = '';
	if($_SERVER['REMOTE_ADDR'])
		$ipaddress = $_SERVER['REMOTE_ADDR'];
	else
		$ipaddress = 'UNKNOWN';
	return $ipaddress;
}

$_c = new _condb;

if (isset($_SESSION["login_name"]) OR isset($_COOKIE['login_name'])) {

	if (isset($_SESSION["login_name"])) {
		$login_name = $_SESSION["login_name"];
	}elseif (isset($_COOKIE['login_name'])) {
		$login_name = $_COOKIE['login_name'];
	}

	$show_select = '';
	$bk = "SELECT * FROM `_lnwphp_user_bank_` WHERE id_user="._user_id_;
	$bq = mysqli_query($_c->d(),$bk);
	while ($br = mysqli_fetch_object($bq)) {
		$show_select .= '<option value="'.$br->id.'">('.$br->acc_bank.')'.$br->acc_name.'</option>';
	}

	$out_select = '';
	$out_bk = "SELECT * FROM `_lnwphp_save_withdrawal_` WHERE id_user="._user_id_;
	$out_bq = mysqli_query($_c->d(),$out_bk);
	while ($out_br = mysqli_fetch_object($out_bq)) {

		$success = ($out_br->success == 0) ? '<span class="label label-default">รอดำเนินการ</span>' : '<span class="label label-success">โอนแล้ว</span>';
		if ($out_br->success == 0) {
			$retVal = '<button class="btn btn-xs btn-danger" onclick="del_post(\''.base64_encode(base64_encode($out_br->id).'f'.base64_encode($out_br->price)).'\')">ลบ</button>';
		}else{
			$retVal = $success;
		}
		$bk_u = "SELECT * FROM `_lnwphp_user_bank_` WHERE id_user="._user_id_." AND id=".$out_br->bank_id;
		$bq_u = mysqli_query($_c->d(),$bk_u);
		$br_u = mysqli_fetch_object($bq_u);
		$out_select .= '<tr id="post_'.base64_encode(base64_encode($out_br->id).'f'.base64_encode($out_br->price)).'">
		<td>'.$out_br->time_log.'</td>
		<td>'.$br_u->acc_bank.'(X'.substr($br_u->acc_numb, -4).')</td>
		<td>'.$out_br->price.'</td>
		<td>'.$success.'</td>
		<td>'.$retVal.'</td>
	</tr>';
}

$_lnwphp_in_th = new __form;
$_s = $_lnwphp_in_th->__form_("withdrawal");

$_sl = new __repl;
$_s = $_sl->__repl_($_s,"{show_withdrawal_select}",$show_select);
$_s = $_sl->__repl_($_s,"{login_name}",($login_name != '' ? $login_name.'" disabled="' : ""));
$_s = $_sl->__repl_($_s,"{email}",(__email__ != '' ? __email__.'" disabled="' : ""));
$_s = $_sl->__repl_($_s,"{terms}",__terms__);
$_s = $_sl->__repl_($_s,"{time_date_now}",date("Y-m-d H:i"));
$_s = $_sl->__repl_($_s,"{ip_user}",get_client_ip());
$_s = $_sl->__repl_($_s,"{user_id}",_user_id_);
$_s = $_sl->__repl_($_s,"{money_point_my_you}",__user_point__);
$_s = $_sl->__repl_($_s,"{list_out_money}",$out_select);

new __show($_s);

}else{

}
?>