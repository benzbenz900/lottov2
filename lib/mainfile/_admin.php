<?php
if (!defined("isdoc")){ //ปิดการเข้าถึงโดยตรงจากไฟล์
	header('HTTP/1.1 404 Not Found');
	echo "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n<html><head>\n<title>404 Not Found</title>\n</head>";
	echo "<body>\n<h1>Not Found T_T *_-</h1>\n<p>The requested URL ".$_SERVER['REQUEST_URI']." was not found on this server.</p>\n";
	echo "<hr>\n".$_SERVER['SERVER_SIGNATURE']."\n</body></html>\n";
	exit;
}
if (isset($_SESSION["login_name"]) OR isset($_COOKIE['login_name'])) {
	if (__isadmin__ == "1") {
		$conn = new _condb;
		$conn = $conn->d();
		$ecs = new ecs;
		if (isset($_GET['cmd'])) {
			$_cmd = $ecs->e($_GET['cmd']);
		}else{
			$_cmd = "admin";
		}
		$_lnwphp_in_th = new __form;
		$_search_admin = $_lnwphp_in_th->__form_($_cmd);
		$_admin_re = new __repl;

		switch ($_cmd) {
			case 'editpost':
			if (isset($_GET['ids'])) {
				$ids = $_GET['ids'];
				if (isset($_POST['save_edit_page'])) {
					$_name = $ecs->e($_POST['name']);
					$_type = $ecs->e($_POST['type']);
					$_detail = $ecs->e($_POST['detail']);

					$_page_sql = "UPDATE `_lnwphp_page_` SET `name` = '$_name', `detail` = '$_detail', `type` = '$_type' WHERE `id` = '$ids'";
					mysqli_query($conn,$_page_sql);
					$admin_alert = '<div class="alert alert-dismissable alert-success">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>สำเร็จ</strong> บันทึกการเปลี่ยนแปลงเรียบร้อยแล้ว</div>';
				}else{
					$admin_alert = "";
				}
				$_lnwphp_sql_page = "SELECT * FROM `_lnwphp_page_` WHERE `id` = '$ids'";
				$_q_lnw_sql = mysqli_query($conn,$_lnwphp_sql_page);
				$_r_lnwphp = mysqli_fetch_array($_q_lnw_sql);
			}else{
				$_r_lnwphp['type'] = "";
				$_r_lnwphp['name'] = "ไม่สามารถแก้ไขได้เนื่องจากไม่พบ ID สำหรับแก้ไข";
				$_r_lnwphp['detail'] = "ไม่สามารถแก้ไขได้เนื่องจากไม่พบ ID สำหรับแก้ไข";
				$admin_alert = '<div class="alert alert-dismissable alert-danger">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>สำเร็จ</strong> ไม่สามารถแก้ไขได้เนื่องจากไม่พบ ID สำหรับแก้ไข</div>';
			}
			$_admin_tpl = $_admin_re->__repl_($_search_admin,"{selected0}",($_r_lnwphp['type'] == '0' ? "selected" : ""));
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{selected1}",($_r_lnwphp['type'] == '1' ? "selected" : ""));
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{selected2}",($_r_lnwphp['type'] == '2' ? "selected" : ""));
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{selected3}",($_r_lnwphp['type'] == '3' ? "selected" : ""));
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{selected4}",($_r_lnwphp['type'] == '4' ? "selected" : ""));
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{selected5}",($_r_lnwphp['type'] == '5' ? "selected" : ""));
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{admin_alert}",$admin_alert);
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{name}",$_r_lnwphp['name']);
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{detail}",$_r_lnwphp['detail']);
			break;
			case 'addslide':
			$_admin_show_alert = "";
			if (isset($_POST['save_addslide'])) {
				$type = explode(".", $_FILES["img"]["name"]);
				$namemovie_img = md5($_FILES["img"]["name"].date("Y-m-d H:i:s")).".".end($type);
				if (file_exists("upload/".$namemovie_img)) {
					echo $namemovie_img." มีไฟล์นี้อยู่แล้ว";
				} else {
					move_uploaded_file($_FILES["img"]["tmp_name"],"upload/".$namemovie_img);
					$name = $ecs->e($_POST['name']);
					$detail = $ecs->e($_POST['detail']);
					$url = $ecs->e($_POST['url']);
					$img = $namemovie_img;
					$show = $ecs->e($_POST['show']);

					$sql_insert_movie = "INSERT INTO `_lnwphp_banner_` (`show_`, `name`, `detail`, `url`, `img`) VALUES ('$show', '$name', '$detail', '$url', '$img');";
					mysqli_query($conn,$sql_insert_movie);
					$_admin_show_alert = '<div class="alert alert-dismissable alert-success">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>สำเร็จ</strong> เพิ่มภาพสไลต์เรียบร้อยแล้ว</div>';
				}
			}

			$sql_admin = "SELECT * FROM `_lnwphp_banner_`";
			$Query_admin = mysqli_query($conn,$sql_admin);
			$_admin_slide_show = "";
			while ($Result_admin = mysqli_fetch_array($Query_admin)) {
				$_admin_slide_show .= '<tr id="slide_'.$Result_admin['id'].'">
				<td>'.$Result_admin['id'].'</td>
				<td>'.$Result_admin['name'].'</td>
				<td>'.$Result_admin['url'].'</td>
				<td><button class="btn btn-xs btn-danger" onclick="del_slide(\''.$Result_admin['id'].'\')">ลบ</button></td>
			</tr>';
		}
		$_admin_tpl = $_admin_re->__repl_($_search_admin,"{slide_show}",$_admin_slide_show);

		$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{admin_alert}",$_admin_show_alert);
		break;
		case 'addcategory':
		if (isset($_GET['ids'])) {
			$ids = $ecs->e($_GET['ids']);
			$sql_admin_e = "SELECT * FROM `_lnwphp_number_type_` WHERE `id` = $ids";
			$Query_admin_e = mysqli_query($conn,$sql_admin_e);
			$Result_admin_e = mysqli_fetch_array($Query_admin_e);

			$_admin_tpl = $_admin_re->__repl_($_search_admin,"{name}",$Result_admin_e['name']);
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{detail}",$Result_admin_e['detail']);
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{time_limit}",$Result_admin_e['ev_t']);
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{time_limitd}",$Result_admin_e['time_limitd']);
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{m_success}",$Result_admin_e['reward']);
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{daybuy}",$Result_admin_e['daybuy']);
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{addcategory_text}",_editcategory_text);
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{addcategory_buttom}",_editcategory_buttom);
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{add_edit}",'edit');
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{numbernub}",$Result_admin_e['numbernub']);
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{numberaut}",$Result_admin_e['numberaut']);
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{set_id}",'+"&ids='.$ids.'"');
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{sty}",'id="edit" style="background-color: antiquewhite;"');
		}else{
			$_admin_tpl = $_admin_re->__repl_($_search_admin,"{name}",'');
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{detail}",'');
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{time_limit}",date("Y-m-d H:i:s"));
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{time_limitd}",date("Y-m-d H:i:s"));
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{daybuy}",date("Y-m-d"));
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{m_success}",'');
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{addcategory_text}",_addcategory_text);
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{addcategory_buttom}",_addcategory_buttom);
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{add_edit}",'add');
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{numbernub}",'');
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{numberaut}",'');
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{set_id}",'');
			$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{sty}",'');
		}
		$timeauto = '';
		for ($i=1; $i <= 24; $i++) {
			$timeauto .= '<option value="'.$i.':00">'.$i.':00</option>';
		}
		$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{showtime}",$timeauto);
		$sql_admin = "SELECT * FROM `_lnwphp_number_type_`";
		$Query_admin = mysqli_query($conn,$sql_admin);
		$_admin_show_alert = "";
		while ($Result_admin = mysqli_fetch_array($Query_admin)) {
			$_admin_show_alert .= '<tr id="post_'.base64_encode($Result_admin['id']).'">
			<td>'.$Result_admin['name'].'</td>
			<td>'.$Result_admin['ev_t'].'</td>
			<td>'.$Result_admin['numberaut'].'</td>
			<td>'.$Result_admin['reward'].'</td>
			<td><a href="./cmd@addcategory.admin@ids='.$Result_admin['id'].'" class="btn btn-xs btn-primary">แก้ไข</a> <button class="btn btn-xs btn-danger" onclick="del_post(\''.base64_encode($Result_admin['id']).'\')">ลบ</button></td>
		</tr>';
	}
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{mycategory}",$_admin_show_alert);
	break;
	case 'listpost':
	$sql_admin = "SELECT * FROM `_lnwphp_page_`";
	$Query_admin = mysqli_query($conn,$sql_admin);
	$_admin_show_alert = "";
	while ($Result_admin = mysqli_fetch_array($Query_admin)) {
		$_admin_show_alert .= '<tr id="post_'.base64_encode($Result_admin['id']).'">
		<td>'.$Result_admin['id'].'</td>
		<td>'.$Result_admin['name'].'</td>
		<td>'.$Result_admin['type'].'</td>
		<td>'.$Result_admin['view'].'</td>
		<td>'.$Result_admin['date'].'</td>
		<td><a href="./cmd@editpost.admin@ids='.$Result_admin['id'].'" class="btn btn-xs btn-primary">แก้ไข</a> <button class="btn btn-xs btn-danger" onclick="del_post(\''.base64_encode($Result_admin['id']).'\')">ลบ</button></td>
	</tr>';
}
$_admin_tpl = $_admin_re->__repl_($_search_admin,"{listpost}",$_admin_show_alert);
break;
case 'addpage':
$admin_alert = "";
if (isset($_GET['ids'])) {
	$ids = $ecs->e($_GET['ids']);
}else{
	$ids = "0";
}
if (isset($_POST['save_addpage'])) {
	$name = $ecs->e($_POST['name']);
	$type = $ecs->e($_POST['type']);
	$detail = $ecs->e($_POST['detail']);
	mysqli_query($conn,"INSERT INTO `_lnwphp_page_` (`name`, `detail`, `view`, `date`, `show_`, `type`) VALUES ('$name', '$detail', '0', CURRENT_TIMESTAMP, '1', '$type')");
	$admin_alert = '<div class="alert alert-dismissable alert-success">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>สำเร็จ</strong> บันทึกการเปลี่ยนแปลงเรียบร้อยแล้ว</div>';
}
$_admin_tpl = $_admin_re->__repl_($_search_admin,"{selected0}",($ids == '0' ? "selected" : ""));
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{selected1}",($ids == '1' ? "selected" : ""));
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{selected2}",($ids == '2' ? "selected" : ""));
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{selected3}",($ids == '3' ? "selected" : ""));
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{selected4}",($ids == '4' ? "selected" : ""));
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{selected5}",($ids == '5' ? "selected" : ""));
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{admin_alert}",$admin_alert);
break;
case 'topuplist':
$sql_admin = "SELECT * FROM `_lnwphp_log_`";
$Query_admin = mysqli_query($conn,$sql_admin);
$_admin_show_alert = "";
while ($Result_admin = mysqli_fetch_object($Query_admin)) {
	$type = ($Result_admin->type == 0) ? "TmTopUp" : "Bank";
	if ($Result_admin->success == 1) {
		$retVal = '<button class="btn btn-xs btn-danger" onclick="del_post(\''.base64_encode($Result_admin->id).'\')">ลบ</button>';
	}else{
		$retVal = '<button id="co_'.$Result_admin->id.'" class="btn btn-xs btn-primary" onclick="update_save(\''.$Result_admin->id.'|'.$Result_admin->price.'|'.$Result_admin->username.'|\')">ยืนยัน</button> <button class="btn btn-xs btn-danger" onclick="del_post(\''.base64_encode($Result_admin->id).'\')">ลบ</button>' ;
	}
	$_admin_show_alert .= '<tr id="post_'.base64_encode($Result_admin->id).'">
	<td>'.$Result_admin->username.'</td>
	<td>'.$Result_admin->price.'</td>
	<td>'.$Result_admin->client_ip.'</td>
	<td>'.$Result_admin->card_password.'</td>
	<td>'.$Result_admin->time_log.'</td>
	<td>'.$type.'</td>
	<td>'.$retVal.'</td>
</tr>';
}
$_admin_tpl = $_admin_re->__repl_($_search_admin,"{topuplist}",$_admin_show_alert);
break;

case 'checknumber':

function sellll($s='',$call='',$ddy='',$type='')
{
	$_c = new _condb;
	if ($call == 1) {
		mysqli_query($_c->d(),"UPDATE `_lnwphp_save_numb_` SET `success` = '2' WHERE `dmy` = '$ecs->e($ddy)' AND `success` != '1' AND `type` == '$ecs->e($type)';");
	}
	$sql = "SELECT * FROM `_lnwphp_save_numb_` $s";
	$QR = mysqli_query($_c->d(),$sql);
	$show_buynumb = "";
	while ($obj = mysqli_fetch_object($QR)) {
		$sqlu = "SELECT * FROM `_lnwphp_accounts_` WHERE `id` = $obj->by_user_id";
		$QRu = mysqli_query($_c->d(),$sqlu);
		$obju = mysqli_fetch_object($QRu);

		$sql_adminc = "SELECT * FROM `_lnwphp_number_type_` WHERE `id` = $obj->type";
		$Query_adminc = mysqli_query($_c->d(),$sql_adminc);
		$obc = mysqli_fetch_object($Query_adminc);

		if ($call == 1) {
			$f = new checkall;
			$obj->success = $f->c($obj->by_user_id, 1,$obj->id,$obj->type);
		}

		if ($obj->success == 1) {
			$cxf = '<span class="label label-success">ถูกรางวัล</span>';
		}elseif ($obj->success == 2) {
			$cxf = '<span class="label label-warning">ไม่ถูกรางวัล</span>';
		}elseif ($obj->success == 3) {
			$cxf = '<span class="label label-warning">รับเงินไปแล้ว</span>';
		}else{
			$cxf = '<button class="btn btn-xs btn-danger" onclick="success('.$obj->by_user_id.', 2, '.$obj->id.');">ไม่ถูกรางวัล</button> <button class="btn btn-xs btn-info" onclick="success('.$obj->by_user_id.', 1 ,'.$obj->id.');">ถูกรางวัล</button>';
		}
		$show_buynumb .= '<tr>
		<td>IDX'.$obj->id.'</td>
		<td>'.$obju->login.'</td>
		<td><u><b>'.$obj->number.'</b></u>|'.$obc->name.'</td>
		<td>'.$obj->salary.'</td>
		<td>'.$obj->dmy.'</td>
		<td>'.$obj->buy_date.'</td>
		<td id="post_'.base64_encode($obj->id).'">'.$cxf.'</td>
	</tr>';
}
return $show_buynumb;
}

function bodydivlnwphp($value='')
{
	$g = '<div class="col-md-8">'.$value.'</div>';
	return $g;
}
$show_buynumb = '';
if (isset($_POST['submit'])) {
	if (!isset($_POST['optiontype']) || $_POST['optiontype'] == "") {
		echo bodydivlnwphp('<div class="alert alert-danger" role="alert">กรุณาเลือกหมวดหมู่ที่จะตรวจเลข</div>');
	} elseif (!isset($_POST['optionku'])) {
		echo bodydivlnwphp('<div class="alert alert-danger" role="alert">กรุณาเลือกงวดวันที่ที่จะตรวจ</div>');
	} elseif (!isset($_POST['optionbu'])) {
		echo bodydivlnwphp('<div class="alert alert-danger" role="alert">กรุณาระบุช่วงเวลาที่ชื่อไม่เกิน</div>');
	} elseif (!isset($_POST['number'])) {
		echo bodydivlnwphp('<div class="alert alert-danger" role="alert">กรุณากรอกเลขที่ต้องการตรวจ</div>');
	}else{
		$optiontype = $ecs->e($_POST['optiontype']);
		$optionku = $ecs->e($_POST['optionku']);
		$optionbu = $ecs->e($_POST['optionbu']);
		$number = $ecs->e($_POST['number']);
		$show_buynumb = sellll("WHERE `number` = '$number' AND `type` = '$optiontype' AND `dmy` = '$optionku' AND `buy_date` < '$optionbu'");
		if (strip_tags($show_buynumb) == "") {
			echo bodydivlnwphp('<div class="alert alert-danger" role="alert">ไม่มีเลขที่ตรงกับเงื่อนไข</div>');
		}
	}
}elseif (isset($_POST['submitall'])){
	if (!isset($_POST['optiontype'])) {
		echo bodydivlnwphp('<div class="alert alert-danger" role="alert">กรุณาเลือกหมวดหมู่ที่จะตรวจเลข</div>');
	} elseif (!isset($_POST['optionku'])) {
		echo bodydivlnwphp('<div class="alert alert-danger" role="alert">กรุณาเลือกงวดวันที่ที่จะตรวจ</div>');
	} elseif (!isset($_POST['optionbu'])) {
		echo bodydivlnwphp('<div class="alert alert-danger" role="alert">กรุณาระบุช่วงเวลาที่ชื่อไม่เกิน</div>');
	} elseif (!isset($_POST['number'])) {
		echo bodydivlnwphp('<div class="alert alert-danger" role="alert">กรุณากรอกเลขที่ต้องการตรวจ</div>');
	}else{
		$optiontype = $ecs->e($_POST['optiontype']);
		$optionku = $ecs->e($_POST['optionku']);
		$optionbu = $ecs->e($_POST['optionbu']);
		$number = $ecs->e($_POST['number']);
		$show_buynumb = sellll("WHERE `number` = '$number' AND `type` = '$optiontype' AND `dmy` = '$optionku' AND `buy_date` < '$optionbu'",'1',$optionku,$optiontype);
		if (strip_tags($show_buynumb) == "") {
			echo bodydivlnwphp('<div class="alert alert-danger" role="alert">ไม่มีเลขที่ตรงกับเงื่อนไข</div>');
		}
	}
}else{
	$show_buynumb = sellll();
}

$_ls = "SELECT * FROM `_lnwphp_number_type_`";
$_qr = mysqli_query($_c->d(),$_ls);
$optiontype = '';
while ($_re = mysqli_fetch_object($_qr)) {
	$optiontype .= '<option value="'.$_re->id.'">'.$_re->name.'</option>';
}

$_admin_tpl = $_admin_re->__repl_($_search_admin,"{optiontype}",$optiontype);
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{allnumber}",$show_buynumb);
break;

case 'saveoutmoney':
$admin_alert = '';
if (isset($_GET['ids'])) {
	$ids = $ecs->e($_GET['ids']);
	if (isset($_POST['cc'])) {
		$_page_sql = "UPDATE `_lnwphp_save_withdrawal_` SET `success` = 1 WHERE `id` = '$ids'";
		mysqli_query($conn,$_page_sql);
		$admin_alert = '<div class="alert alert-dismissable alert-success">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>สำเร็จ</strong> บันทึกการเปลี่ยนแปลงเรียบร้อยแล้ว</div>';
	}else{
		$admin_alert = '<div class="alert alert-dismissable alert-danger">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<form method="post" action="./cmd@saveoutmoney.admin@ids='.$ids.'">แน่ใจหรือไม่ว่าต้องการ ตั้งสถานะรายการนี้เป็นโอนเงินแล้ว<br>
			<button name="cc" type="submit" class="btn btn-danger">แน่ใจ</button> <a href="./cmd@saveoutmoney.admin" class="btn btn-success">ยกเลิก</a></form></div>';
		}
	}
	$out_select = '';
	$out_bk = "SELECT * FROM `_lnwphp_save_withdrawal_`"; 
	$out_bq = mysqli_query($conn,$out_bk); 
	while ($out_br = mysqli_fetch_object($out_bq)) {

		$success = ($out_br->success == 0) ? '<span class="label label-default">รอดำเนินการ</span>' : '<span class="label label-success">โอนแล้ว</span>';
		if ($out_br->success == 0) {
			$retVal = '<a href="./cmd@saveoutmoney.admin@ids='.$out_br->id.'" class="btn btn-xs btn-primary">ยืนยัน</a> <button class="btn btn-xs btn-danger" onclick="del_post(\''.base64_encode(base64_encode($out_br->id).'f'.base64_encode($out_br->price)).'\')">ลบ</button>';
		}else{
			$retVal = $success ;
		}
		$bk_u = "SELECT * FROM `_lnwphp_user_bank_` WHERE `id_user` = '$out_br->id_user'"; 
		$bq_u = mysqli_query($conn,$bk_u); 
		$br_u = mysqli_fetch_object($bq_u);
		$out_select .= '<tr id="post_'.base64_encode($out_br->id).'">
		<td>'.$out_br->user.'</td>
		<td>'.$br_u->acc_name.'</td>
		<td>'.$out_br->time_log.'</td>
		<td>'.$br_u->acc_bank.'('.$br_u->acc_numb.')</td>
		<td>'.$out_br->price.'</td>
		<td>'.$success.'</td>
		<td>'.$retVal.'</td>
	</tr>';
}
$_admin_tpl = $_admin_re->__repl_($_search_admin,"{showalert}",$admin_alert);
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{saveoutmoney}",$out_select);
break;

case 'usermanager':

$item_per_page = 10;
$page_url = "cmd@usermanager.admin@ids=";
$namepage = '';

if(isset($_GET["ids"])){
	$page_number = filter_var($_GET["ids"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
	if(!is_numeric($page_number)){die('Invalid page number!');}
}else{
	$page_number = 1;
}

$page_position = (($page_number-1) * $item_per_page);

$out_selectu = '';
$sqlacu = "SELECT * FROM `_lnwphp_accounts_` LIMIT $page_position,$item_per_page";
$QRacu = mysqli_query($_c->d(),$sqlacu);
while ($objacu = mysqli_fetch_object($QRacu)) {
	if ($objacu->isadmin == 1) {
		$a = '<span class="label label-success">Admin</span>';
	}elseif ($objacu->isadmin == 2) {
		$a = '<span class="label label-danger">Block</span>';
	}else{
		$a = '<span class="label label-default">User</span>';
	}
	$out_selectu .= '<tr id="post_'.base64_encode($objacu->id).'">
	<td>'.$objacu->id.'</td>
	<td>'.$objacu->login.'</td>
	<td>'.$objacu->email.'</td>
	<td>'.$objacu->created_time.'</td>
	<td>'.$objacu->point.'</td>
	<td>'.$a.'</td>
	<td><a href="./cmd@edituser.admin@ids='.$objacu->id.'" class="btn btn-xs btn-info">แก้ไข</a> <button class="btn btn-xs btn-danger" onclick="del_post(\''.base64_encode($objacu->id).'\')">ลบ</button></td>
</tr>';
}

$sqlacus = "SELECT COUNT(*) AS total FROM `_lnwphp_accounts_`";
$QRacus = mysqli_query($_c->d(),$sqlacus);
$get_total_rows = mysqli_fetch_object($QRacus);
$total_pages = ceil($get_total_rows->total/$item_per_page);

$lnwphppaginate = paginate($item_per_page, $page_number, $get_total_rows->total, $total_pages, $page_url,$namepage);

$_admin_tpl = $_admin_re->__repl_($_search_admin,"{showuser}",$out_selectu);
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{paginate}",$lnwphppaginate);
break;

case 'edituser':
if (isset($_GET['ids'])) {
	$ids = $ecs->e($_GET['ids']);

	$out_selectu = '';

	if (isset($_POST['save_user'])) {
		$email = mysqli_real_escape_string($_c->d(),$_POST['email']);
		$point = mysqli_real_escape_string($_c->d(),$_POST['point']);
		$status = mysqli_real_escape_string($_c->d(),$_POST['status']);
		mysqli_query($_c->d(),"UPDATE `_lnwphp_accounts_` SET `email` = '$email', `point` = '$point', `isadmin` = '$status' WHERE `id` = $ids;");
		$out_selectu = '<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>สำเร็จ</strong> แก้ไข User เรียบร้อยแล้ว</div>';
	}

	
	$sqlacu = "SELECT * FROM `_lnwphp_accounts_` WHERE `id` = $ids";
	$QRacu = mysqli_query($_c->d(),$sqlacu);
	$objacu = mysqli_fetch_object($QRacu);
	$_admin_tpl = $_admin_re->__repl_($_search_admin,"{email}",$objacu->email);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{loginname}",$objacu->login);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{point}",$objacu->point);
	

	if ($objacu->isadmin == 1) {
		$a = 'value="1">Admin';
	}elseif ($objacu->isadmin == 2) {
		$a = 'value="2">Block';
	}else{
		$a = 'value="0">User';
	}

	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{status}",$a);

	$sql = "SELECT * FROM `_lnwphp_save_numb_` WHERE `by_user_id` = $ids";
	$QR = mysqli_query($_c->d(),$sql);
	$show_buynumb = "";
	while ($obj = mysqli_fetch_object($QR)) {
		$show_buynumb .= '<tr id="post_'.base64_encode(base64_encode($obj->id).'f'.base64_encode($obj->salary)).'">
		<td>IDX'.$obj->id.'</td>
		<td>'.$obj->number.'</td>
		<td>'.$obj->salary.'</td>
		<td>'.$obj->dmy.'</td>
		<td>'.$obj->buy_date.'</td>
		<td><button class="btn btn-xs btn-danger" onclick="del_post(\''.base64_encode(base64_encode($obj->id).'f'.base64_encode($obj->salary)).'\')">ลบ</button></td>
	</tr>';
}
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{buy_menu}",$show_buynumb);

$sql_admina = "SELECT * FROM `_lnwphp_log_` WHERE `username` = '$objacu->login'";
$Query_admina = mysqli_query($_c->d(),$sql_admina);
$_admin_show_alert = "";
while ($Result_admin = mysqli_fetch_object($Query_admina)) {
	$type = ($Result_admin->type == 0) ? "TmTopUp" : "Bank";
	if ($Result_admin->success == 1) {
		$retVal = '<button class="btn btn-xs btn-danger" onclick="del_postt(\''.base64_encode($Result_admin->id).'\')">ลบ</button>';
	}else{
		$retVal = '<button id="co_'.$Result_admin->id.'" class="btn btn-xs btn-primary" onclick="update_save(\''.$Result_admin->id.'|'.$Result_admin->price.'|'.$Result_admin->username.'|\')">ยืนยัน</button> <button class="btn btn-xs btn-danger" onclick="del_post(\''.base64_encode($Result_admin->id).'\')">ลบ</button>' ;
	}
	$_admin_show_alert .= '<tr id="post_'.base64_encode($Result_admin->id).'">
	<td>'.$Result_admin->username.'</td>
	<td>'.$Result_admin->price.'</td>
	<td>'.$Result_admin->client_ip.'</td>
	<td>'.$Result_admin->card_password.'</td>
	<td>'.$Result_admin->time_log.'</td>
	<td>'.$type.'</td>
	<td>'.$retVal.'</td>
</tr>';
}
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{topuplist}",$_admin_show_alert);

$out_select = '';

$out_bk = "SELECT * FROM `_lnwphp_save_withdrawal_` WHERE `id_user` = $ids"; 
$out_bq = mysqli_query($conn,$out_bk); 
while ($out_br = mysqli_fetch_object($out_bq)) {

	$success = ($out_br->success == 0) ? '<span class="label label-default">รอดำเนินการ</span>' : '<span class="label label-success">โอนแล้ว</span>';
	if ($out_br->success == 0) {
		$retVal = '<a href="./cmd@saveoutmoney.admin@ids='.$out_br->id.'" class="btn btn-xs btn-primary">ยืนยัน</a> <button class="btn btn-xs btn-danger" onclick="del_postw(\''.base64_encode(base64_encode($out_br->id).'f'.base64_encode($out_br->price)).'\')">ลบ</button>';
	}else{
		$retVal = $success ;
	}
	$bk_u = "SELECT * FROM `_lnwphp_user_bank_`"; 
	$bq_u = mysqli_query($conn,$bk_u); 
	$br_u = mysqli_fetch_object($bq_u);
	$out_select .= '<tr id="post_'.base64_encode($out_br->id).'">
	<td>'.$br_u->acc_name.'</td>
	<td>'.$out_br->time_log.'</td>
	<td>'.$br_u->acc_bank.'('.$br_u->acc_numb.')</td>
	<td>'.$out_br->price.'</td>
	<td>'.$success.'</td>
	<td>'.$retVal.'</td>
</tr>';
}
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{saveoutmoney}",$out_select);

}else{
	$out_selectu = '<div class="alert alert-warning alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong>ไม่พบ User ID</strong> กรุณาลองอีกครั้ง</div>';
	$_admin_tpl = $_admin_re->__repl_($_search_admin,"{email}",'');
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{loginname}",'');
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{point}",'');
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{status}",'');
}
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{showalert}",$out_selectu);
break;

case 'tmtopup':
$out_selectu = '';
$sqlacu = "SELECT * FROM `_lnwphp_point_`";
$QRacu = mysqli_query($_c->d(),$sqlacu);
while ($objacu = mysqli_fetch_object($QRacu)) {
	$out_selectu .= '<tr id="post_'.base64_encode($objacu->id).'">
	<td>'.$objacu->vipname.'</td>
	<td>'.$objacu->vipprice.'</td>
	<td>'.$objacu->point.'</td>
	<td><a href="./cmd@edittmtopup.admin@ids='.$objacu->id.'" class="btn btn-xs btn-info">แก้ไข</a> <button class="btn btn-xs btn-danger" onclick="del_post(\''.base64_encode($objacu->id).'\')">ลบ</button></td>
</tr>';
}
$_admin_tpl = $_admin_re->__repl_($_search_admin,"{showuser}",$out_selectu);
break;

//
//

case 'edittmtopup':
if (isset($_GET['ids'])) {
	$ids = $ecs->e($_GET['ids']);

	$out_selectu = '';

	if (isset($_POST['save_user'])) {
		$vipname = mysqli_real_escape_string($_c->d(),$_POST['vipname']);
		$vipprice = mysqli_real_escape_string($_c->d(),$_POST['vipprice']);
		$point = mysqli_real_escape_string($_c->d(),$_POST['point']);
		mysqli_query($_c->d(),"UPDATE `_lnwphp_point_` SET `vipname` = '$vipname', `vipprice` = '$vipprice', `point` = '$point' WHERE `id` = $ids;");
		$out_selectu = '<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>สำเร็จ</strong> แก้ไขบัตรทรูเรียบร้อยแล้ว</div>';
	}

	$sqlacu = "SELECT * FROM `_lnwphp_point_` WHERE `id` = $ids";
	$QRacu = mysqli_query($_c->d(),$sqlacu);
	$objacu = mysqli_fetch_object($QRacu);
	$_admin_tpl = $_admin_re->__repl_($_search_admin,"{vipname}",$objacu->vipname);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{vipprice}",$objacu->vipprice);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{point}",$objacu->point);

}else{
	$out_selectu = '<div class="alert alert-warning alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong>ไม่พบ User ID</strong> กรุณาลองอีกครั้ง</div>';
	$_admin_tpl = $_admin_re->__repl_($_search_admin,"{email}",'');
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{loginname}",'');
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{point}",'');
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{status}",'');
}
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{showalert}",$out_selectu);
break;

//
//


case 'setting':
$folder = new __list_folder_;
$folder = $folder->listFolders(_theme_);
$folderc = '';
if (isset($_POST['save_setting'])) {
	$_post_webname = $ecs->e($_POST['webname']);
	$_post_foot_text = $ecs->e($_POST['foot_text']);
	$_post_fan_page = $ecs->e($_POST['fan_page']);
	$_post_code_stat = $ecs->e($_POST['code_stat']);
	$_post_banner_show = $ecs->e($_POST['banner_show']);
	$_post_terms = $ecs->e($_POST['terms']);
	$_post_headcode = $ecs->e($_POST['headcode']);
	$_post_footcode = $ecs->e($_POST['footcode']);
	$_post_tmtopup_uid = $ecs->e($_POST['tmtopup_uid']);
	$_post_percent = $ecs->e($_POST['percent']);
	$commissions_day = $ecs->e($_POST['commissions_day']);
	$_post_tmtopup_passkey = $ecs->e($_POST['tmtopup_passkey']);
	$_post_cancel_time = $ecs->e($_POST['cancel']);
	if ($_FILES["image_header"]["name"] == "") {
		$image_header = __image_header__;
	} else {
		$type_image_header = explode(".", $_FILES["image_header"]["name"]);
		$image_header = md5($_FILES["image_header"]["name"].time()).".".end($type_image_header);
		if (file_exists("upload/".$image_header)) {
			echo $image_header." "._admin_text_0020_;
		} else {
			@unlink("upload/".__image_header__);
			move_uploaded_file($_FILES["image_header"]["tmp_name"],"upload/".$image_header);
		}
	}

	$name_image_header = $ecs->e($_POST['name_image_header']);
	$theme = $ecs->e($_POST['theme']);

	foreach ($folder as $key => $value) {
		$value = explode('/', $value);
		$value = end($value);
		if ($theme == $value) {
			$selected = 'selected="selected"';
		}else{
			$selected = '';
		}
		$folderc .= '<option '.$selected.' value="'.$value.'">'.$value.'</option>';
	}

	$_setting_sql = "UPDATE `_lnwphp_setting_` SET
	`webname` = '$_post_webname',
	`foot_text` = '$_post_foot_text',
	`fan_page` = '$_post_fan_page',
	`code_stat` = '$_post_code_stat',
	`banner_show` = '$_post_banner_show',
	`terms` = '$_post_terms',
	`headcode` = '$_post_headcode',
	`footcode` = '$_post_footcode',
	`image_header` = '$image_header',
	`name_image_header` = '$name_image_header',
	`theme` = '$theme',
	`tmtopup_uid` = '$_post_tmtopup_uid',
	`tmtopup_passkey` = '$_post_tmtopup_passkey',
	`percent` = '$_post_percent',
	`commissions_day` = '$commissions_day',
	`cancel_time` = '$_post_cancel_time'";
	mysqli_query($conn,$_setting_sql);
	$_admin_show_alert = '<div class="alert alert-dismissable alert-success">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>สำเร็จ</strong> บันทึกการเปลี่ยนแปลงเรียบร้อยแล้ว</div>';
	$_admin_tpl = $_admin_re->__repl_($_search_admin,"{show_alert}",$_admin_show_alert);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{webname}",$_post_webname);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{foot_text}",$_post_foot_text);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{fan_page}",$_post_fan_page);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{code_stat}",$_post_code_stat);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{terms}",$_POST['terms']);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{headcode}",$_post_headcode);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{footcode}",$_post_footcode);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{image_header}",$image_header);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{name_image_header}",$name_image_header);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{list_theme}",$folderc);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{tmtopup_uid}",$_post_tmtopup_uid);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{percent}",$_post_percent);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{tmtopup_passkey}",$_post_tmtopup_passkey);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{commissions_day}",$commissions_day);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{tmtopup_url_api}",_urlconfig_.'?api=tmtopupapi');
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{cancel}",$_post_cancel_time);
}else{
	$_admin_show_alert = "";
	$_admin_tpl = $_admin_re->__repl_($_search_admin,"{show_alert}",$_admin_show_alert);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{webname}",__webname__);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{foot_text}",__footer_text__);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{fan_page}",__fanpage__);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{code_stat}",__code_stat_);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{terms}",__terms__);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{headcode}",__headcode__);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{footcode}",__footcode__);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{name_image_header}",__name_image_header__);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{image_header}",__image_header__);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{tmtopup_uid}",__tmtopup_uid__);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{percent}",_post_percent);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{tmtopup_passkey}",__tmtopup_passkey__);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{commissions_day}",commissions_day);
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{tmtopup_url_api}",_urlconfig_.'?api=tmtopupapi');
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{cancel}",cancel);

	foreach ($folder as $key => $value) {
		$value = explode('/', $value);
		$value = end($value);
		if (_themename_ == $value) {
			$selected = 'selected="selected"';
		}else{
			$selected = '';
		}
		$folderc .= '<option '.$selected.' value="'.$value.'">'.$value.'</option>';
	}
	$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{list_theme}",$folderc);
}
break;
case 'addadminbank':
$sql_admin = "SELECT * FROM `_lnwphp_user_bank_`";
$Query_admin = mysqli_query($_c->d(),$sql_admin);
$_admin_show_alert = "";
while ($Result_admin = mysqli_fetch_object($Query_admin)) {
	$retVal = '<button class="btn btn-xs btn-danger" onclick="del_post(\''.base64_encode($Result_admin->id).'\')">ลบ</button>';
	$_admin_show_alert .= '<tr id="post_'.($Result_admin->id).'">
	<td>'.$Result_admin->acc_name.'</td>
	<td>'.$Result_admin->acc_type.'</td>
	<td>'.$Result_admin->acc_bank.'</td>
	<td>'.$Result_admin->acc_numb.'</td>
	<td>'.$retVal.'</td>
</tr>';
}

$_admin_tpl = $_admin_re->__repl_($_search_admin,"{admin_bank}",$_admin_show_alert);
break;
default:
$_admin_menu_1 = '<a class="list-group-item" href="./cmd@setting.admin"><span class="glyphicon glyphicon-cog"></span> ตั้งค่าเว็บ</a>
<a class="list-group-item list-group-item-success" href="./cmd@topuplist.admin"><span class="glyphicon glyphicon-th-list"></span> รายการเติมเงิน</a>
<a class="list-group-item" href="./cmd@addpage.admin@ids=0"><span class="glyphicon glyphicon-th-large"></span> เพิ่มหน้าเว็บ</a>
<a class="list-group-item" href="./cmd@listpost.admin"><span class="glyphicon glyphicon-th-list"></span> จัดการหน้าเว็บ</a>
<a class="list-group-item" href="./cmd@addadminbank.admin"><span class="glyphicon glyphicon-th-list"></span> เพิ่มบัญชีธนาคาร</a>
<!--<a class="list-group-item list-group-item-info" href="./cmd@loadnumber.admin"><span class="glyphicon glyphicon-th-list"></span> โหลดโพย</a>-->';

$_admin_menu_2 = '<a class="list-group-item list-group-item-success" href="./cmd@saveoutmoney.admin"><span class="glyphicon glyphicon-th-large"></span> รายการแจ้งถอนเงิน</a>
<!--<a class="list-group-item list-group-item-info" href="./cmd@addslide.admin"><span class="glyphicon glyphicon-th"></span> เพิ่มสไลต์</a>-->
<a class="list-group-item list-group-item-success" href="./cmd@addcategory.admin"><span class="glyphicon glyphicon-th"></span> เพิ่มหมวดหมุ่เลข</a>
<a class="list-group-item" href="./cmd@usermanager.admin"><span class="glyphicon glyphicon-th"></span> จัดการสมาชิก</a>
<a class="list-group-item list-group-item-success" href="./cmd@checknumber.admin"><span class="glyphicon glyphicon-th"></span> ตรวจเลขที่ถูก</a>
<a class="list-group-item" href="./cmd@tmtopup.admin"><span class="glyphicon glyphicon-th"></span> แก้ไขราคาบัตรทรู</a>
<!--<a class="list-group-item list-group-item-info" href="./cmd@messageuser.admin"><span class="glyphicon glyphicon-th"></span> ข้อความจากสมาชิก</a>-->';
if (isset($_POST['save_admin'])) {
	$_post_post_alert = $ecs->e($_POST['post_alert']);
	$_setting_sql = "UPDATE `_lnwphp_setting_` SET
	`post_alert` = '$_post_post_alert'";
	mysqli_query($conn,$_setting_sql);
	$_admin_show_alert = '<div class="alert alert-dismissable alert-success">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>สำเร็จ</strong> บันทึกการเปลี่ยนแปลงเรียบร้อยแล้ว</div>';
}else{
	$_admin_show_alert = '';
}
$_admin_tpl = $_admin_re->__repl_($_search_admin,"{post_alert}",__post_alert__);
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{admin_menu_1}",$_admin_menu_1);
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{admin_menu_2}",$_admin_menu_2);
$_admin_tpl = $_admin_re->__repl_($_admin_tpl,"{admin_alert}",$_admin_show_alert);
break;
}

new __show($_admin_tpl);

}else{
	header( "location: userpage.html" );
}

}else{
	header( "location: login.html" );
}
?>