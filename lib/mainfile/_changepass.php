<?php
if (!defined("isdoc")){ //ปิดการเข้าถึงโดยตรงจากไฟล์
	header('HTTP/1.1 404 Not Found');
	echo "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n<html><head>\n<title>404 Not Found</title>\n</head>";
	echo "<body>\n<h1>Not Found</h1>\n<p>The requested URL ".$_SERVER['REQUEST_URI']." was not found on this server.</p>\n";
	echo "<hr>\n".$_SERVER['SERVER_SIGNATURE']."\n</body></html>\n";
	exit;
}
if (isset($_SESSION["login_name"]) OR isset($_COOKIE['login_name'])) {
$ecs = new ecs;
function change_pass_form($epo="",$ep1="",$ep2="",$show_alert="")
{
	$ecs = new ecs;
	$_lnwphp_in_th = new __form;
	$_search = $_lnwphp_in_th->__form_("changepass");

	$_lnwphp_re = new __repl;
	$_tpl_show = $_lnwphp_re->__repl_($_search,"{show_alert}",$ecs->e($show_alert));
	$_tpl_show = $_lnwphp_re->__repl_($_tpl_show,"{epo}",$ecs->e($epo));
	$_tpl_show = $_lnwphp_re->__repl_($_tpl_show,"{ep1}",$ecs->e($ep1));
	$_tpl_show = $_lnwphp_re->__repl_($_tpl_show,"{ep2}",$ecs->e($ep2));

	new __show($_tpl_show);
}

$_lnwphp_conn = new _condb;
$conn = $_lnwphp_conn->d();

if (isset($_POST['password_old']) && isset($_POST['password_new1']) && isset($_POST['password_new2'])) {
	$password_old = $ecs->e($_POST['password_old']);
	$password_new1 = $ecs->e($_POST['password_new1']);
	$password_new2 = $ecs->e($_POST['password_new2']);
	if ($password_new1 == $password_new2) {
		$_lnwphp_password = new __encrypt;
		$password_old = $_lnwphp_password->_encrypt($password_old);

		if (isset($_SESSION["login_name"]) OR isset($_COOKIE['login_name'])) {
			if (isset($_SESSION["login_name"])) {
				$login_name = $_SESSION["login_name"];
			}elseif (isset($_COOKIE['login_name'])) {
				$login_name = $_COOKIE['login_name'];
			}
		}

		$sqlchk = "SELECT * FROM `_lnwphp_accounts_` WHERE `login` = '$login_name'";
		$resultchk = mysqli_query($conn,$sqlchk);
		$Resulchk = mysqli_fetch_array($resultchk);
		if ($Resulchk['password'] == $password_old) {
			$password_new = $_lnwphp_password->_encrypt($password_new1);
			mysqli_query($conn,"UPDATE `_lnwphp_accounts_` SET `password` = '$password_new' WHERE `login` = '$login_name'");
			$show_alert = '<div class="alert alert-dismissable alert-success">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<strong>สำเร็จ</strong> เปลี่ยนรหสผ่านใหม่เรียบร้อยแล้ว</div>';
			change_pass_form("","","",$show_alert);
		}else{
			$show_alert = '<div class="alert alert-dismissable alert-warning">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<strong>โอ้ๆๆ</strong> รหัสผ่านเก่าไม่ถูกค้อง กรุณาลองใหม่อีกครั้ง</div>';
			change_pass_form("has-error","","",$show_alert);
		}
	}else{
		$show_alert = '<div class="alert alert-dismissable alert-warning">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>โอ้ๆๆ</strong> รหัสผ่านใหม่ไม่ตรงกัน กรุณาลองใหม่อีกครั้ง</div>';
		change_pass_form("","has-error","has-error",$show_alert);
	}
}else{
	change_pass_form();
}
}else{
	header( "location: login.html" );
}
?>