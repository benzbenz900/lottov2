<?php
if (!defined("isdoc")){header('HTTP/1.1 404 Not Found');echo "Not Link Direct File"; exit;}

class __send_mail
{
	function __construct($t="",$s="",$b="")
	{
		$t = __send_mail::xss_cleane($t);
		$s = __send_mail::xss_cleane($s);
		$b = __send_mail::xss_cleane($b);
		require_once( _include_.'/class.phpmailer.php');
		$mail = new PHPMailer();
		$mail->IsHTML(true);
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = "ssl";
		$mail->Host = "smtp.gmail.com";
		$mail->Port = 587;
		$mail->Username = __Gmail_Email__;
		$mail->Password = __Gmail_Passwprd__;
		$mail->From = __Gmail_Email__;
		$mail->AddReplyTo = __Gmail_Email__;
		$mail->FromName = __From_Name__;
		$mail->Subject = $s;
		$mail->Body = $b;
		$mail->AddAddress($t);
		$mail->set('X-Priority', '1');
		if(!$mail->Send()) {
			$strTo = $t;
			$strSubject = $s;
			$strHeader = "Content-type: text/html; charset=UTF-8\r\n";
			$strHeader .= "From: ".__Gmail_Email__."\r\nReply-To: ".__Gmail_Email__;
			$strMessage = $b;
			$flgSend = @mail($strTo,$strSubject,$strMessage,$strHeader);
			if(!$flgSend)
			{
				echo 'Error: ' . $mail->ErrorInfo;
			}
		}
		$mail->SmtpClose();
	}


	function xss_cleane($data)
	{
		$data = strip_tags($data);
		$data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
		$data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
		$data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
		$data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

		$data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);


		$data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
		$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
		$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);


		$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
			$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
				$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

				$data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

				do{

					$old_data = $data;
					$data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
				}
				while ($old_data !== $data);

				return $data;
			}

}
?>