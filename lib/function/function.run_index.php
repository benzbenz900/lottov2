<?php
if (!defined("isdoc")){header('HTTP/1.1 404 Not Found');echo "Not Link Direct File"; exit;}

class __run_index
{
	function __construct()
	{
		if (strnatcmp(phpversion(),'5.0') < 0){
			die("PHP Version ".phpversion()." < 5.0 Please update PHP To 5.0 or Newer");
		}

		include _include_.'/_header.php';
		if (!isset($_GET['page'])) {
			include _include_.'/_slidershow.php';
		}
		include _include_.'/_menu.php';

		if (isset($_GET['page']) && $_GET['page'] != "logout") {
			if (isset($_SESSION["login_name"]) OR isset($_COOKIE['login_name']) OR $_GET['page'] == "register" || $_GET['page'] == "resetpassword" || $_GET['page'] == "login" || $_GET['page'] == "page" || $_GET['page'] == "event" || $_GET['page'] == "cons" || $_GET['page'] == "rates") {
				include _mainfile_.'/_'.$_GET['page'].'.php';
			}else{
				header( "location: "._urlconfig_."register.html" );
			}
		}elseif(isset($_GET['page']) && $_GET['page'] == "logout"){
			session_destroy();
			setcookie("login_name", "", time() - (86400 * 30), "/");
			header( "location: "._urlconfig_."index.html" );
		}else{
			include _mainfile_.'/_homepage.php';
		}

		include _include_.'/_footer.php';
	}

}
class lnw_run
{
	function __construct()
	{
		if (isset($_GET['api'])) {
			$lnwphpinth = new __run_api;
			$lnwphpinth->__run_api_(lnw_run::xss_clean($_GET['api']));
		}elseif(isset($_GET['action']) && $_GET['action'] == "del" && isset($_GET['d']) && isset($_GET['t']) && isset($_GET['i'])){
			new __del(lnw_run::xss_clean($_GET['d']),lnw_run::xss_clean($_GET['t']),lnw_run::xss_clean($_GET['i']));
		}elseif(isset($_GET['action']) && $_GET['action'] == "save_in_money" && isset($_GET['u']) && isset($_GET['e']) && isset($_GET['p']) && isset($_GET['t']) && isset($_GET['b']) && isset($_GET['i'])){
			new __save_tmb(lnw_run::xss_clean($_GET['u']),lnw_run::xss_clean($_GET['e']),lnw_run::xss_clean($_GET['p']),lnw_run::xss_clean($_GET['t']),lnw_run::xss_clean($_GET['b']),lnw_run::xss_clean($_GET['i']));
		}elseif(isset($_GET['action']) && $_GET['action'] == "save_out_money" && isset($_GET['uid']) && isset($_GET['u']) && isset($_GET['e']) && isset($_GET['p']) && isset($_GET['t']) && isset($_GET['b']) && isset($_GET['i'])){
			new __save_widaw(lnw_run::xss_clean($_GET['uid']),lnw_run::xss_clean($_GET['u']),lnw_run::xss_clean($_GET['e']),lnw_run::xss_clean($_GET['p']),lnw_run::xss_clean($_GET['t']),lnw_run::xss_clean($_GET['b']),lnw_run::xss_clean($_GET['i']));
		}elseif(isset($_GET['action']) && $_GET['action'] == "save_mybank" && isset($_GET['id_user']) && isset($_GET['acc_name']) && isset($_GET['acc_numb']) && isset($_GET['acc_type']) && isset($_GET['acc_bank'])){
			new __save_mybank(lnw_run::xss_clean($_GET['id_user']),lnw_run::xss_clean($_GET['acc_name']),lnw_run::xss_clean($_GET['acc_numb']),lnw_run::xss_clean($_GET['acc_type']),lnw_run::xss_clean($_GET['acc_bank']));
		}elseif(isset($_GET['action']) && $_GET['action'] == "save_adminbank" && isset($_GET['acc_name']) && isset($_GET['acc_numb']) && isset($_GET['acc_type']) && isset($_GET['acc_bank'])){
			new __save_adminbank(lnw_run::xss_clean($_GET['acc_name']),lnw_run::xss_clean($_GET['acc_numb']),lnw_run::xss_clean($_GET['acc_type']),lnw_run::xss_clean($_GET['acc_bank']));
		}elseif(isset($_GET['action']) && $_GET['action'] == "edit_category" && isset($_GET['name']) && isset($_GET['detail']) && isset($_GET['time']) && isset($_GET['day']) && isset($_GET['mmm']) && isset($_GET['ids']) && isset($_GET['nub']) && isset($_GET['aut']) && isset($_GET['daybuy'])){
			new __edit_category(lnw_run::xss_clean($_GET['name']),lnw_run::xss_clean($_GET['detail']),lnw_run::xss_clean($_GET['time']),lnw_run::xss_clean($_GET['day']),lnw_run::xss_clean($_GET['mmm']),lnw_run::xss_clean($_GET['ids']),lnw_run::xss_clean($_GET['nub']),lnw_run::xss_clean($_GET['aut']),lnw_run::xss_clean($_GET['daybuy']));
		}elseif(isset($_GET['action']) && $_GET['action'] == "add_category" && isset($_GET['name']) && isset($_GET['detail']) && isset($_GET['time']) && isset($_GET['day']) && isset($_GET['mmm']) && isset($_GET['nub']) && isset($_GET['aut']) && isset($_GET['daybuy'])){
			new __add_category(lnw_run::xss_clean($_GET['name']),lnw_run::xss_clean($_GET['detail']),lnw_run::xss_clean($_GET['time']),lnw_run::xss_clean($_GET['day']),lnw_run::xss_clean($_GET['mmm']),lnw_run::xss_clean($_GET['nub']),lnw_run::xss_clean($_GET['aut']),lnw_run::xss_clean($_GET['daybuy']));
		}elseif(isset($_GET['action']) && $_GET['action'] == "save_check" && isset($_GET['uid']) && isset($_GET['sid']) && isset($_GET['nid'])){
			new __save_check(lnw_run::xss_clean($_GET['uid']),lnw_run::xss_clean($_GET['sid']),lnw_run::xss_clean($_GET['nid']));
		}elseif(isset($_GET['action']) && $_GET['action'] == "gethttp" && isset($_GET['id'])){
			new __gethttp(lnw_run::xss_clean($_GET['id']));
		}elseif(isset($_GET['action']) && $_GET['action'] == "load_table_widaw"){
			$dlt = new __load_table;
			$dlt = $dlt->_d();
		}elseif(isset($_GET['action']) && $_GET['action'] == "load_mybank"){
			$dlt = new __load_mybank;
			$dlt = $dlt->_d();
		}elseif(isset($_GET['action']) && $_GET['action'] == "load_category"){
			$dlt = new __load_category;
			$dlt = $dlt->_d();
		}elseif(isset($_GET['action']) && $_GET['action'] == "sql" && isset($_GET['sql']) && isset($_GET['point']) && isset($_GET['uid'])){
			new __sql_in(lnw_run::xss_clean($_GET['sql']),lnw_run::xss_clean($_GET['point']),lnw_run::xss_clean($_GET['uid']));
		}else{
			new __run_index;
		}
	}



	function xss_clean($data)
	{
		$ec = new ecs;
		$data = $ec->e($data);
		$data = strip_tags($data);
		$data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
		$data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
		$data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
		$data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

		$data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);


		$data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
		$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
		$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);


		$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
			$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
				$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

				$data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

				do{

					$old_data = $data;
					$data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
				}
				while ($old_data !== $data);

				return $data;
			}
		}
		?>