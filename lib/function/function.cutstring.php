<?php
class __cut_str
{
	function __cut_str_($str, $maxChars='', $holder=''){

		if (strlen($str) > $maxChars ){
			$str = iconv_substr($str, 0, $maxChars,"UTF-8") . $holder;
		}
		return $str;
	}
}

?>