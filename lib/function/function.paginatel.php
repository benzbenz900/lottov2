<?php
function paginate($item_per_page, $current_page, $total_records, $total_pages, $page_url,$namepage)
{
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){
        $pagination .= '<ul class="pagination">';

        $right_links    = $current_page + 3;
        $previous       = 3 - $current_page;
        $next           = $current_page + 1;
        $first_link     = true;

        if($current_page > 1){
            $previous_link = ($previous==0)?1:$previous;
            $pagination .= '<li class="first"><a href="'.$page_url.'1'.$namepage.'" title="First">&laquo;</a></li>';
            $pagination .= '<li><a href="'.$page_url.$previous_link.$namepage.'" title="Previous">&lt;</a></li>';
            for($i = ($current_page-2); $i < $current_page; $i++){
                if($i > 0){
                    $pagination .= '<li><a href="'.$page_url.$i.$namepage.'">'.$i.'</a></li>';
                }
            }
            $first_link = false;
        }
        if($first_link){
            $pagination .= '<li class="first active"><a href="'.$page_url.$current_page.$namepage.'">'.$current_page.'</a></li>';
        }elseif($current_page == $total_pages){
            $pagination .= '<li class="last active"><a href="'.$page_url.$current_page.$namepage.'">'.$current_page.'</a></li>';
        }else{
            $pagination .= '<li class="active"><a href="'.$page_url.$current_page.$namepage.'">'.$current_page.'</a></li>';
        }

        for($i = $current_page+1; $i < $right_links ; $i++){
            if($i<=$total_pages){
                $pagination .= '<li><a href="'.$page_url.$i.$namepage.'">'.$i.'</a></li>';
            }
        }
        if($current_page < $total_pages){
            $next_link = ($i > $total_pages)? $total_pages : $i;
            $pagination .= '<li><a href="'.$page_url.$next_link.$namepage.'" >&gt;</a></li>';
                $pagination .= '<li class="last"><a href="'.$page_url.$total_pages.$namepage.'" title="Last">&raquo;</a></li>'; //last link
            }

            $pagination .= '</ul>';
        }
        return $pagination;
    }
    ?>