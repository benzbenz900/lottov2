<?php
if (!defined("isdoc")){header('HTTP/1.1 404 Not Found');echo "Not Link Direct File"; exit;}
include 'include.function.php';

new run_function;

include _lang_.'/th_TH.php';

$_c = new _condb;

$_set = "SELECT * FROM `_lnwphp_setting_` where `id` = '1'";
$_qr = mysqli_query($_c->d(),$_set);
$_ru = mysqli_fetch_object($_qr);

if (isset($_SESSION["login_name"]) OR isset($_COOKIE['login_name'])) {
	if (isset($_SESSION["login_name"])) {
		define('_user_id_', $_SESSION["user_id"]);
		define('__login_name__', '<span class="glyphicon glyphicon-user"></span> '.$_SESSION["login_name"]);
		define('__email__', $_SESSION["email"]);
		define('__date_register__', $_SESSION["date_register"]);
		define('__isadmin__', $_SESSION["isadmin"]);
		define('__user_point__', $_SESSION["user_point"]);
		define('_phone', $_SESSION['phone']);
		define('ref', $_SESSION['ref']);
	}elseif (isset($_COOKIE['login_name'])) {
		define('_user_id_', $_COOKIE["user_id"]);
		define('__login_name__', '<span class="glyphicon glyphicon-user"></span> '.$_COOKIE['login_name']);
		define('__email__', $_COOKIE['email']);
		define('__date_register__', $_COOKIE['date_register']);
		define('__isadmin__', $_COOKIE['isadmin']);
		define('__user_point__', $_COOKIE['user_point']);
		define('_phone', $_COOKIE['phone']);
		define('ref', $_COOKIE['ref']);
	}
}else{
	define('__login_name__', '<a href="./register.html" class="btn btn-sm btn-primary">Register</a> <a href="./login.html" class="btn btn-sm btn-success">Login</a>');
	define('__email__', "");
	define('__date_register__', "");
}

define('_title_', $_ru->webname);
define('__webname__', $_ru->webname);
define('__fanpage__', $_ru->fan_page);
define('_themename_', $_ru->theme);
define('__footer_text__', $_ru->foot_text);
define('_fanpage_', $_ru->fan_page);
define('__banner_show_', $_ru->banner_show);
define('__code_stat_', $_ru->code_stat);
define('__terms__', $_ru->terms);
define('_head_code_', $_ru->headcode);
define('__headcode__', $_ru->headcode);
define('__footcode__', $_ru->footcode);
define('__name_image_header__', $_ru->name_image_header);
define('__image_header__', $_ru->image_header);
define('__post_alert__', $_ru->post_alert);
define('__tmtopup_uid__', $_ru->tmtopup_uid);
define('__tmtopup_passkey__', $_ru->tmtopup_passkey);
define('_post_percent', $_ru->percent);
define('commissions_day', $_ru->commissions_day);
define('cancel', $_ru->cancel_time);
?>